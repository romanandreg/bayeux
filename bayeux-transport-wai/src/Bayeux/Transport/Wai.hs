{-# LANGUAGE OverloadedStrings #-}
module Bayeux.Transport.Wai
       ( BayeuxTransportOptions (..)
       , defaultOptions
       , newBayeuxMiddleware
       , bayeuxHandler
       , module Tiempo ) where

import Control.Monad       (mplus)
import Control.Monad.Error (ErrorT (..))
import Control.Monad.Trans (MonadIO (..))

import qualified Blaze.ByteString.Builder   as Blaze
import qualified Data.Aeson                 as Aeson
import qualified Data.ByteString.Char8      as B8
import qualified Data.ByteString.Lazy.Char8 as LB8
import qualified Data.Text.Encoding         as Text

import Data.List   (foldl1', isPrefixOf)
import Data.Monoid (Monoid (..))
import Data.String (fromString)
import Data.Text   (Text)

import           Data.Default                      (def)
import qualified Data.Vault.Lazy                   as Vault
import qualified Network.HTTP.Types.Status         as Wai
import qualified Network.HTTP.Types.URI            as URI
import qualified Network.Wai                       as Wai
import qualified Network.Wai.Middleware.Static     as Wai
import qualified Network.Wai.Session               as Wai
import qualified Network.Wai.Session.ClientSession as Wai
import           Web.ClientSession                 (getDefaultKey)


import qualified Bayeux.Assets                        as Assets
import           Bayeux.Storage.Core                  (processHandshake)
import           Bayeux.Transport.Core                (defaultOptions)
import           Bayeux.Transport.Wai.CallbackPolling
import           Bayeux.Transport.Wai.LongPolling
import           Bayeux.Transport.Wai.Types

import Bayeux.Types
import Tiempo       (TimeInterval, minutes, seconds, toNominalDiffTime)

--------------------------------------------------------------------------------
-- Bayeux Request Parsers

parseBayeuxRequestList :: BayeuxRequestParser
parseBayeuxRequestList req =
  foldl1' mplus $ map ($ req) [ callbackPollingParser, longPollingParser ]

--------------------------------------------------------------------------------
-- Bayeux Response Serializers

serializeBayeuxResponse :: ConnectionType
                        -> [Response Aeson.Value]
                        -> Wai.Response
serializeBayeuxResponse LongPolling respList =
  longPollingSerializer respList
serializeBayeuxResponse (CallbackPolling jsonP) respList =
  callbackPollingSerializer jsonP respList
serializeBayeuxResponse connType _ =
  Wai.responseLBS Wai.status400 []
    $ mconcat ["bayeuxResponseListSerializer: connType '"
              , fromString $ show connType
              , "' not supported" ]

--------------------------------------------------------------------------------
-- [Private] Middleware Assets

newBayeuxAssetsMiddleware
  :: [Text] -> BayeuxTransportOptions -> IO Wai.Middleware
newBayeuxAssetsMiddleware bayeuxPath opts = do
   bayeuxClientFilePath <- getBayeuxClientPath
   bayeuxSourceMapFilePath <- Assets.getBayeuxClientSourceMapPath
   return
     $ Wai.staticPolicy
     $ Wai.only [ (assetPath, bayeuxClientFilePath)
                , (sourceMapPath, bayeuxSourceMapFilePath) ]
  where
    getBayeuxClientPath
      | _bayeuxMinifyJS opts = Assets.getMinifiedBayeuxClientPath
      | otherwise = Assets.getBayeuxClientPath
    assetPath =
      B8.unpack
        $ B8.tail
        $ Blaze.toByteString
        $ URI.encodePath (bayeuxPath ++ ["client.js"]) []
    sourceMapPath = assetPath ++ ".map"

--------------------------------------------------------------------------------
-- [Public] Bayeux Middleware

newBayeuxMiddleware :: PubSub -> BayeuxTransportOptions -> WaiMonad Wai.Middleware
newBayeuxMiddleware pubsub opts = do
    bayeuxApp <- mkBayeuxApp
    let middleware waiApp req =
          if bayeuxPath `isPrefixOf` Wai.pathInfo req
            then bayeuxApp req
            else waiApp req

    return middleware
  where
    bayeuxPath =
      fst $ URI.decodePath
          $ Text.encodeUtf8
          $ _bayeuxEndpoint opts
    mkBayeuxApp = do
      session        <- liftIO Vault.newKey
      store          <- fmap Wai.clientsessionStore getDefaultKey
      let sessionM = Wai.withSession store (_bayeuxCookieName opts) def session
      assetsM  <- liftIO $ newBayeuxAssetsMiddleware bayeuxPath opts
      return . assetsM . sessionM $ bayeuxHandler session pubsub


--------------------------------------------------------------------------------
-- [Public] Bayeux Handler

bayeuxHandler
  :: Vault.Key (Wai.Session IO String String)
  -> PubSub
  -> Wai.Request
  -> WaiMonad Wai.Response
bayeuxHandler sessionVault pubsub waiReq = do
    result <- runErrorT $ do
      parsedReqList <- parseBayeuxRequestList waiReq
      ErrorT $ uncurry handleRequests parsedReqList

    case result of
      Right (connType, respList) ->
        return $ serializeBayeuxResponse connType respList
      Left errMsg ->
        return $
          Wai.responseLBS Wai.status400
                          []
                          (LB8.fromChunks . (:[]) $ B8.pack errMsg)
  where
    handleRequests :: ConnectionType
                   -> [Request Aeson.Value]
                   -> WaiMonad (Either String (ConnectionType, [Response Aeson.Value]))
    handleRequests connType [req@(ReqHandshake {})] = do
      mResp <- liftIO $ processHandshake (_pubsubStorage pubsub) req
      case mResp of
        Just resp -> return $! Right (connType, [resp])
        Nothing -> return $! Left "Bayeux: Handshake timeout"

    handleRequests LongPolling reqList =
      let Just session = Vault.lookup sessionVault (Wai.vault waiReq)
      in longPollingHandler session pubsub LongPolling reqList

    handleRequests connType@(CallbackPolling _) reqList =
      let Just session = Vault.lookup sessionVault (Wai.vault waiReq)
      in callbackPollingHandler session pubsub connType reqList

    handleRequests connType _ =
      return $! Left
             $ "Bayeux: connType '"
               ++ show connType ++ "' not supported"

--------------------------------------------------------------------------------
