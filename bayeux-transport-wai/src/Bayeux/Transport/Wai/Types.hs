{-# LANGUAGE CPP            #-}
{-# LANGUAGE PackageImports #-}
module Bayeux.Transport.Wai.Types where

import "mtl" Control.Monad.Error (ErrorT)

import qualified Data.Aeson as Aeson

import qualified Network.Wai as Wai

import Bayeux.Types

-- #if MIN_VERSION_wai(2,0,0)
type WaiMonad = IO
type ErrorWaiMonad = ErrorT String IO
-- #else
-- import Data.Conduit (ResourceT)

-- type WaiMonad = ResourceT IO
-- type ErrorWaiMonad = ErrorT String (ResourceT IO)
-- #endif

type BayeuxRequestParser =
  Wai.Request -> ErrorWaiMonad (ConnectionType, [Request Aeson.Value])
