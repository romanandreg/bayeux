{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PackageImports    #-}
{-# LANGUAGE TemplateHaskell   #-}
module Bayeux.Transport.Wai.Polling where

import Control.Lens

import Control.Applicative      ((<$>), (<|>))
import Control.Concurrent       (threadDelay)
import Control.Concurrent.Async (async, wait)
import Control.Monad.Trans      (MonadIO (..))

import qualified Data.Aeson                 as Aeson
import           Data.ByteString.Char8      (ByteString)
import qualified Data.ByteString.Lazy.Char8 as LB8
import qualified Data.Text                  as Text

import qualified Network.Wai         as Wai
import qualified Network.Wai.Parse   as Wai
import qualified Network.Wai.Session as Wai

import           Rx.Observable (Observable)
import qualified Rx.Observable as Observable

import qualified Rx.Logger as Logger

import qualified Tiempo

import Bayeux.Response     (isRepublishResponse)
import Bayeux.Storage.Core (clientErrorObservable, getClientObservable,
                            handleRequest, keepClientAlive)

import Bayeux.Transport.Wai.Types
import Bayeux.Types

--------------------------------------------------------------------------------

getContentType :: Wai.Request -> Maybe ByteString
getContentType req =
    fmap (fst . Wai.parseContentType)
         (lookup "content-type" $ Wai.requestHeaders req)

maybeToEither :: String -> Maybe a -> ErrorWaiMonad a
maybeToEither errMsg = maybe (fail errMsg) return

bsToBayeuxRequests :: ByteString -> Maybe [Request Aeson.Value]
bsToBayeuxRequests = Aeson.decode . LB8.fromChunks . (:[])

getClientId :: [Request Aeson.Value] -> Maybe ClientId
getClientId = firstOf (traverse . requestClientId)

--------------------------------------------------------------------------------

pollingClientResponseObservable
  :: PubSub
  -> ClientId
  -> WaiMonad (Maybe (Observable WaiMonad JSONResponse))
pollingClientResponseObservable pubsub clientId =
    fmap (fmap observableDecoration)
         (getClientObservable pubsub clientId)
  where
    observableDecoration =
      -- NOTE: This a keep alive, not a sleep
      -- Second timeout ensures that we always
      -- reply to UA even if there is no
      -- new messages, this way we avoid the UA
      -- sending parallel requests
      Observable.timeout (Tiempo.seconds 10)
      -- First timeout ensures we reply to User-Agent (UA)
      -- as soon as there is something to send
      . Observable.timeoutAfterFirstSelector isRepublishResponse
                                             (Tiempo.microSeconds 1000)

pollingClientErrorObservable
  :: PubSub
  -> ClientId
  -> Observable WaiMonad JSONResponse
pollingClientErrorObservable pubsub clientId =
  Observable.timeoutAfterFirst (Tiempo.microSeconds 100) $
    clientErrorObservable pubsub clientId


getLastClientMessages :: PubSub
                      -> ClientId
                      -> WaiMonad (Maybe [JSONResponse])
getLastClientMessages pubsub clientId = do
  keepClientAlive pubsub clientId

  let errorObservable = pollingClientErrorObservable pubsub clientId
  mResponseObservable <- pollingClientResponseObservable pubsub clientId

  let mObservable = mResponseObservable <|> Just errorObservable

  case mObservable of
    Nothing ->  return Nothing
    Just observable -> do
      eResult <- Observable.toList observable
      case eResult of
        Right result
          | null result -> return Nothing
          | otherwise -> return $! Just result
        Left (result, _)
          | null result -> return Nothing
          | otherwise -> return $! Just result

--------------------------------------------------------------------------------

-- | If Connect response has an UnknownClientId error we need to
-- guarantee that a new Handshake happens only once, this is possible
-- only if the clientId stored in the bayeux cookie is the same found
-- in the UnknownClientId record, otherwise a new Handshake has been
-- established and we don't need to do another Handshake
noneAdviceDecorator :: Wai.Session IO String String
                    -> PubSub
                    -> Observable WaiMonad (Response Aeson.Value)
                    -> Observable WaiMonad (Response Aeson.Value)
noneAdviceDecorator (lookupSession, _) _pubsub =
    Observable.mapM decorator
  where
    decorator resp =
      case _responseError resp of
        Just UnknownClientId -> do
          liftIO $ putStrLn "noneAdviceDecorator: response with UnknwonClientId error"
          let unknownClientId = resp ^. responseClientId
          currentClientId <- liftIO $ maybe "" Text.pack <$> lookupSession "cid"
          -- NOTE: When reconnecting from server down we need to check if
          -- currentClientId is the same as the one that is on the
          -- session, if that is the case, the advice should be whatever
          -- was there before, otherwise, do an advice reconnect None
          if currentClientId == unknownClientId
            then do
              liftIO $ putStrLn "Leaving advice unchanged"
              return resp
            else do
              liftIO $ putStrLn "Setting advice to None"
              return $ resp
                     & responseAdvice . adviceReconnect
                     .~ Just None
        _ -> do
          liftIO $ putStrLn "noneAdviceDecorator: response with no error"
          return resp

--------------------------------------------------------------------------------

withClientId
  :: ConnectionType
  -> [Request Aeson.Value]
  -> (ClientId
      -> ConnectionType
      -> [Request Aeson.Value]
      -> WaiMonad [Response Aeson.Value])
  -> WaiMonad (Either String (ConnectionType, [Response Aeson.Value]))
withClientId connectionType reqList action = do
  let mClientId = getClientId reqList
  case mClientId of
    Just clientId -> do
      respList <- action clientId connectionType reqList
      return $! Right (connectionType, respList)
    Nothing -> return $! Left "Bayeux: clientId expected"

pollingHandler
  :: Wai.Session WaiMonad String String
  -> PubSub
  -> ClientId
  -> ConnectionType
  -> [Request Aeson.Value]
  -> WaiMonad [Response Aeson.Value]
pollingHandler _session pubsub clientId connType reqList = do
    clientMsgsAsync <- async $ getLastClientMessages pubsub clientId
    liftIO $ threadDelay 100
    mapM_ handleRequest' reqList

    clientMsgs <- liftIO $ wait clientMsgsAsync
    case clientMsgs of
      Just msgs -> return msgs
      Nothing -> return []
  where
    handleRequest' req0@(ReqConnect {}) = do
      let req = req0 & requestConnectionType .~ connType
      liftIO $ handleRequest pubsub req
    handleRequest' req = liftIO $ handleRequest pubsub req
