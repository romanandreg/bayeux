{-# LANGUAGE OverloadedStrings #-}
module Bayeux.Transport.Wai.CallbackPolling where

import Control.Applicative (pure, (<$>), (<*>))
import Control.Monad       (join)

import qualified Data.Aeson                 as Aeson
import           Data.ByteString.Char8      (ByteString)
import qualified Data.ByteString.Lazy.Char8 as LB8
import           Data.Monoid                (mconcat)

import qualified Network.HTTP.Types.Method as Wai
import qualified Network.HTTP.Types.Status as Wai
import qualified Network.Wai               as Wai
import qualified Network.Wai.Session       as Wai

import Bayeux.Transport.Wai.Polling
import Bayeux.Transport.Wai.Types
import Bayeux.Types

--------------------------------------------------------------------------------

callbackPollingParser :: BayeuxRequestParser
callbackPollingParser req
    | Wai.requestMethod req == Wai.methodGet = do
      let qs = Wai.queryString req
          jsonp = join $ lookup "jsonp" qs
          messageVal = join $ lookup "message" qs
      (,) <$> (pure . CallbackPolling $ maybe "jsonpcallback" id jsonp)
          <*> (maybeToEither "callback-polling: message param has invalid JSON"
                             (messageVal >>= bsToBayeuxRequests))
    | otherwise = fail "callback-polling: expecting HTTP GET method"

callbackPollingSerializer :: ByteString -> [Response Aeson.Value] -> Wai.Response
callbackPollingSerializer jsonP respList =
  let result = mconcat [ LB8.fromChunks [jsonP]
                       , "("
                       , Aeson.encode respList
                       , ");" ]
  in Wai.responseLBS Wai.status200
                     [("content-type", "text/javascript")]
                     result

callbackPollingHandler :: Wai.Session IO String String
                       -> PubSub
                       -> ConnectionType
                       -> [Request Aeson.Value]
                       -> WaiMonad (Either String
                                           (ConnectionType, [Response Aeson.Value]))
callbackPollingHandler session pubsub connType@(CallbackPolling {}) reqList =
  withClientId connType reqList (pollingHandler session pubsub)
callbackPollingHandler _ _ _ _ =
  error "callbackPollingHandler: being used with invalid connectionType"
