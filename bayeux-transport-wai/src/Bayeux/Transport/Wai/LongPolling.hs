{-# LANGUAGE OverloadedStrings #-}
module Bayeux.Transport.Wai.LongPolling where

import Control.Applicative (pure, (<$>), (<*>))
import Control.Monad.Trans (lift)

import qualified Data.Aeson   as Aeson
import           Data.Conduit (($$))
import qualified Data.Conduit as Conduit

import qualified Network.HTTP.Types.Method as Wai
import qualified Network.HTTP.Types.Status as Wai
import qualified Network.Wai               as Wai
import qualified Network.Wai.Parse         as Wai
import qualified Network.Wai.Session       as Wai

import Bayeux.Transport.Wai.Polling
import Bayeux.Transport.Wai.Types
import Bayeux.Types

--------------------------------------------------------------------------------

longPollingParser :: BayeuxRequestParser
longPollingParser req
  | Wai.requestMethod req == Wai.methodPost = do
      let mContentType = getContentType req
      case mContentType of
        Just "application/json" -> do
          bodyContent <- lift $ Wai.requestBody req $$ Conduit.await
          (,) <$> pure LongPolling
              <*> maybeToEither "long-polling: message has invalid JSON"
                                (bodyContent >>= bsToBayeuxRequests)
        Just "application/x-www-form-urlencoded" -> do
          (args, _) <- lift $ Wai.parseRequestBody Wai.lbsBackEnd req
          let messageVal = lookup "message" args
          (,) <$> pure LongPolling
              <*> maybeToEither "long-polling: message has invalid JSON"
                                (messageVal >>= bsToBayeuxRequests)
        _ -> fail "long-polling: invalid content-type"
  | otherwise = fail "long-polling: expecting HTTP POST method"

longPollingSerializer :: [Response Aeson.Value] -> Wai.Response
longPollingSerializer respList =
  Wai.responseLBS Wai.status200
                  [("content-type", "application/json")]
                  $ Aeson.encode respList

longPollingHandler :: Wai.Session IO String String
                   -> PubSub
                   -> ConnectionType
                   -> [Request Aeson.Value]
                   -> WaiMonad (Either String (ConnectionType, [Response Aeson.Value]))
longPollingHandler session pubsub LongPolling reqList =
  withClientId LongPolling reqList (pollingHandler session pubsub)
longPollingHandler _ _ _ _ =
  error "longPollingHandler: being used with invalid connectionType"
