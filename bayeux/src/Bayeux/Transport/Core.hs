{-# LANGUAGE OverloadedStrings #-}
module Bayeux.Transport.Core
       ( BayeuxTransportOptions(..)
       , defaultOptions ) where

import Tiempo (seconds)

import Bayeux.Types

defaultOptions :: BayeuxTransportOptions
defaultOptions =
  BayeuxTransportOptions
    "/bayeux" (seconds 10) "bayeux" True
