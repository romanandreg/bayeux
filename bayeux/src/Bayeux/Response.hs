module Bayeux.Response where

import Data.Maybe (isNothing)
import Bayeux.Types

isValidResponse :: Response a -> Bool
isValidResponse (RespHandshake _ (Right _) _) = True
isValidResponse resp = isNothing $ _responseError resp

isConnectResponse :: Response a -> Bool
isConnectResponse (RespConnect {}) = True
isConnectResponse _ = False

isDisconnectResponse :: Response a -> Bool
isDisconnectResponse (RespDisconnect {}) = True
isDisconnectResponse _ = False

isRepublishResponse :: Response a -> Bool
isRepublishResponse (RespRepublish {}) = True
isRepublishResponse _ = False
