module Bayeux.Assets where

import Paths_bayeux (getDataFileName)

getMinifiedBayeuxClientPath :: IO FilePath
getMinifiedBayeuxClientPath =
  getDataFileName "faye/browser/faye-browser-min.js"

getBayeuxClientSourceMapPath :: IO FilePath
getBayeuxClientSourceMapPath =
  getDataFileName "faye/browser/faye-browser-min.js.map"

getBayeuxClientPath :: IO FilePath
getBayeuxClientPath =
  getDataFileName "faye/browser/faye-browser.js"
