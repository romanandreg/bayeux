{-# LANGUAGE DeriveDataTypeable         #-}
{-# LANGUAGE ExistentialQuantification  #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE PackageImports             #-}
{-# LANGUAGE RankNTypes                 #-}
{-# LANGUAGE RecordWildCards            #-}
{-# LANGUAGE TemplateHaskell            #-}
module Bayeux.Types where

import Control.Applicative (Applicative, pure, (<$>), (<*>))
import Control.Lens        hiding ((.=))

import "mtl" Control.Monad.Error (Error)

import           Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as HashMap


import           Data.Aeson               (FromJSON (..), ToJSON (..), (.:),
                                           (.:?), (.=))
import qualified Data.Aeson               as Aeson
import qualified Data.Aeson.Encode.Pretty as Aeson
import qualified Data.Aeson.Lens          as Aeson

import Data.ByteString (ByteString)
import Data.Hashable   (Hashable)
import Data.String     (IsString)
import Data.Text       (Text)
import Data.Time       (UTCTime)
import Data.Typeable   (Typeable)

import qualified Data.ByteString.Char8      as B8
import qualified Data.ByteString.Lazy.Char8 as LB8
import           Data.Set                   (Set)
import qualified Data.Text                  as Text
import qualified Data.Text.Encoding         as Text
import qualified Data.Text.Lazy             as LText

import qualified Tiempo as Tiempo

import qualified Rx.Disposable as Disposable
import           Rx.Logger     (Buildable, HasLogger (..), Logger,
                                ToLogMsg (..))
import           Rx.Observable (Observable)
import           Rx.Subject    (Subject)

--------------------------------------------------------------------------------

data GenericPayload
  = forall a. (FromJSON a, ToJSON a, Typeable a)
    => GenericPayload { fromGenericPayload :: a }
  deriving (Typeable)

instance Show GenericPayload  where
  show payload = show $ Aeson.encode payload

instance ToJSON GenericPayload where
  toJSON (GenericPayload payload) = toJSON payload

newtype ChannelName
  = ChannelName { fromChannelName :: Text }
  deriving (Show, Eq, Ord, Typeable, IsString, FromJSON, ToJSON, Buildable)

newtype ChannelPattern
  = ChannelPattern { fromChannelPattern :: Text }
  deriving (Show, Eq, Ord, Typeable, IsString, FromJSON, ToJSON, Hashable, Buildable)

type ErrorMsg = Text

data BayeuxError
  = UnknownClientId
  deriving (Show)

instance Error BayeuxError

instance Aeson.ToJSON BayeuxError where
  toJSON UnknownClientId = Aeson.String "unknown clientId"

type ClientId = Text
type MsgId = Text

data PubSubMsg a
  = PubSubMsg {
    _msgClientId :: ClientId
  , _msgChanName :: ChannelName
  , _msgId       :: MsgId
  , _msgPayload  :: a
  }
  deriving (Show)

type GenericPubSubMsg = PubSubMsg GenericPayload

--------------------------------------------------------------------------------

data ConnectionType
  = LongPolling
  | CallbackPolling ByteString
  | WebSocket
  | EventSource
  | InProcess
  deriving (Show, Ord)

isInProcessConnectionType :: ConnectionType -> Bool
isInProcessConnectionType InProcess = True
isInProcessConnectionType _ = False

isPollingConnectionType :: ConnectionType -> Bool
isPollingConnectionType LongPolling = True
isPollingConnectionType (CallbackPolling _) = True
isPollingConnectionType _ = False

instance Eq ConnectionType where
  LongPolling == LongPolling = True
  (CallbackPolling _) == (CallbackPolling _) = True
  WebSocket == WebSocket = True
  EventSource == EventSource = True
  InProcess == InProcess = True
  _ == _ = False

instance ToJSON ConnectionType where
  toJSON LongPolling = toJSON ("long-polling" :: Text)
  toJSON (CallbackPolling _) = toJSON ("callback-polling" :: Text)
  toJSON WebSocket = toJSON ("websocket" :: Text)
  toJSON EventSource = toJSON ("eventsource" :: Text)
  toJSON InProcess =
    error "toJSON[Bayeux.ConnectionType]: 'InProcess' must not be serialized"

instance FromJSON ConnectionType where
  parseJSON (Aeson.String "long-polling") = return LongPolling
  parseJSON (Aeson.String "callback-polling") =
    return (CallbackPolling "jsonpcallback")
  parseJSON (Aeson.String "websocket") = return WebSocket
  parseJSON (Aeson.String "eventsource") = return EventSource
  parseJSON val =
    fail $ "FromJSON[Bayeux.ConnectionType]: connection type '"
         ++ show val
         ++ "' not recognized"

supportedConnectionTypes :: [ConnectionType]
supportedConnectionTypes = [CallbackPolling "", LongPolling, WebSocket]

--------------------------------------------------------------------------------

data ReconnectAdvice
  = Retry
  | Handshake
  | None
  deriving (Show, Eq, Ord)

instance Aeson.ToJSON ReconnectAdvice where
  toJSON Retry = Aeson.toJSON ("retry" :: Text)
  toJSON Handshake = Aeson.toJSON ("handshake" :: Text)
  toJSON None = Aeson.toJSON ("none" :: Text)

newtype IntervalAdvice
  = IntervalAdvice { fromIntervalAdvice :: Tiempo.TimeInterval }

instance Aeson.ToJSON IntervalAdvice where
  toJSON (IntervalAdvice time) =
    toJSON (floor $ Tiempo.toMilliSeconds time :: Int)

data Advice
  = Advice {
    _adviceReconnect       :: Maybe ReconnectAdvice
  , _adviceInterval        :: Maybe IntervalAdvice
  , _adviceMultipleClients :: Maybe Bool
  }

emptyAdvice :: Advice
emptyAdvice = Advice Nothing Nothing Nothing

$(makeLenses ''Advice)

instance Aeson.ToJSON Advice where
  toJSON (Advice mreconnect minterval mmultipleClients) =
      foldr step json0 [ ("reconnect", toJSON <$> mreconnect)
                       , ("interval" , toJSON <$> minterval)
                       , ("multiple-clients", toJSON <$> mmultipleClients) ]
    where
      json0 = Aeson.object []
      step (field, mvalue) json =
        maybe json
              (\value -> json & Aeson._Object . at field
                              .~ Just value)
              mvalue

--------------------------------------------------------------------------------

data Request a
  = ReqHandshake {
    _requestJSON            :: Aeson.Value
  , _requestMsgId           :: Maybe MsgId
  , _requestInitialClientId :: Maybe ClientId
  }
  | ReqConnect {
    _requestJSON           :: Aeson.Value
  , _requestMsgId          :: Maybe MsgId
  , _requestClientId       :: ClientId
  , _requestConnectionType :: ConnectionType
  }
  | ReqDisconnect {
    _requestJSON     :: Aeson.Value
  , _requestMsgId    :: Maybe MsgId
  , _requestClientId :: ClientId
  }
  | ReqSubscribe {
    _requestJSON                    :: Aeson.Value
  , _requestMsgId                   :: Maybe MsgId
  , _requestClientId                :: ClientId
  , _requestSubscribeChannelPattern :: ChannelPattern
  , _requestSubscribeHandler        :: Maybe (GenericPubSubMsg -> IO ())
  }
  | ReqUnsubscribe {
    _requestJSON                      :: Aeson.Value
  , _requestMsgId                     :: Maybe MsgId
  , _requestClientId                  :: ClientId
  , _requestUnsubscribeChannelPattern :: ChannelPattern
  }
  | ReqPublish {
    _requestJSON               :: Aeson.Value
  , _requestMsgId              :: Maybe MsgId
  , _requestClientId           :: ClientId
  , _requestPublishChannelName :: ChannelName
  , _requestPublishPayload     :: a
  }

$(makeLenses ''Request)

instance Aeson.ToJSON a => Show (Request a) where
  show req = Text.unpack
             . Text.decodeUtf8
             . B8.concat
             . LB8.toChunks
             $ Aeson.encodePretty req

instance Functor Request where
  fmap f req@(ReqPublish {}) = req & requestPublishPayload %~ f
  fmap _ (ReqHandshake {..}) = ReqHandshake {..}
  fmap _ (ReqConnect {..}) = ReqConnect {..}
  fmap _ (ReqDisconnect {..}) = ReqDisconnect {..}
  fmap _ (ReqSubscribe {..}) = ReqSubscribe {..}
  fmap _ (ReqUnsubscribe {..}) = ReqUnsubscribe {..}

instance Eq a => Eq (Request a) where
  req1 == req2 =
    let tuple = (,) <$> _requestMsgId req1 <*> _requestMsgId req2
    in case tuple of
      Just (reqId1, reqId2) -> reqId1 == reqId2
      Nothing -> _requestJSON req1 == _requestJSON req2

instance (ToJSON a) => ToJSON (Request a) where
  toJSON (ReqPublish json _ _ _ payload) =
    case json ^? Aeson._Object . at "data" of
      Just (Just _) -> toJSON json
      _ ->
        toJSON $ json
               & Aeson._Object . at "data"
               .~ (Just $ toJSON payload)
  toJSON req = toJSON $ _requestJSON req

instance (FromJSON a) => FromJSON (Request a) where
  parseJSON object@(Aeson.Object o) = do
    channel <- o .: "channel"
    case channel of
      "/meta/handshake" ->
        ReqHandshake <$> pure object
                     <*> o .:? "id"
                     <*> pure Nothing
      "/meta/connect" ->
        ReqConnect <$> pure object
                   <*> o .:? "id"
                   <*> o .: "clientId"
                   <*> o .: "connectionType"
      "/meta/disconnect" ->
        ReqDisconnect <$> pure object
                      <*> o .: "id"
                      <*> o .: "clientId"
      "/meta/subscribe" ->
        ReqSubscribe <$> pure object
                     <*> o .: "id"
                     <*> o .: "clientId"
                     <*> o .: "subscription"
                     <*> pure Nothing
      "/meta/unsubscribe" ->
        ReqUnsubscribe <$> pure object
                       <*> o .: "id"
                       <*> o .: "clientId"
                       <*> o .: "subscription"
      _ -> do
        payload <- o .: "data"
        ReqPublish <$> pure object
                   <*> o .: "id"
                   <*> o .: "clientId"
                   <*> pure (ChannelName channel)
                   <*> pure payload

  parseJSON json =
    fail $ "FromJSON[Bayeux.Request]: Expecting Object got '"
         ++ show json
         ++ "' instead"

data Response a
  = RespHandshake    { _responseRequest         :: Request a
                     , _responseErrorOrClientId :: Either ErrorMsg ClientId
                     , _responseAdvice          :: Advice }

  | RespConnect      { _responseRequest :: Request a
                     , _responseError   :: Maybe BayeuxError
                     , _responseAdvice  :: Advice }

  | RespDisconnect   { _responseRequest :: Request a
                     , _responseError   :: Maybe BayeuxError
                     , _responseAdvice  :: Advice }

  | RespSubscribe    { _responseRequest :: Request a
                     , _responseError   :: Maybe BayeuxError
                     , _responseAdvice  :: Advice }

  | RespUnsubscribe { _responseRequest :: Request a
                    , _responseError   :: Maybe BayeuxError
                    , _responseAdvice  :: Advice }

  | RespPublish      { _responseRequest :: Request a
                     , _responseError   :: Maybe BayeuxError
                     , _responseAdvice  :: Advice }

  | RespRepublish    { _responseRequest         :: Request a
                     , _republishClientIdTarget :: ClientId }

  deriving (Typeable)

$(makeLenses ''Response)

responseClientId :: (Applicative f, Functor f)
                 => (ClientId -> f ClientId) -> Response a -> f (Response a)
responseClientId = responseRequest . requestClientId

responseMsgId :: (Applicative f, Functor f)
                 => (Maybe MsgId -> f (Maybe MsgId)) -> Response a -> f (Response a)
responseMsgId = responseRequest . requestMsgId

instance Eq a => Eq (Response a) where
  resp1 == resp2 = _responseRequest resp1 == _responseRequest resp2

instance Functor Response where
  fmap f resp = resp & responseRequest %~ fmap f

instance ToJSON a => ToJSON (Response a) where
  toJSON (RespHandshake req (Left errMsg) advice) =
    let json = _requestJSON req
    in json & Aeson._Object . at "successful" .~ Just (Aeson.Bool False)
            & Aeson._Object . at "error" .~ Just (Aeson.toJSON errMsg)
            & Aeson._Object . at "advice" .~ Just (Aeson.toJSON advice)
            & Aeson._Object . at "supportedConnectionTypes"
              .~ Just (Aeson.toJSON supportedConnectionTypes)
  toJSON (RespHandshake req (Right cid) advice) =
    let json = _requestJSON req
    in json & Aeson._Object . at "clientId" .~ Just (Aeson.String cid)
            & Aeson._Object . at "successful" .~ Just (Aeson.Bool True)
            & Aeson._Object . at "advice" .~ Just (Aeson.toJSON advice)
            & Aeson._Object . at "supportedConnectionTypes"
              .~ Just (Aeson.toJSON supportedConnectionTypes)

  toJSON (RespConnect req err advice) =
    let json = _requestJSON req
    in case err of
      Just errMsg ->
        json & Aeson._Object . at "successful" .~ Just (Aeson.Bool False)
             & Aeson._Object . at "error" .~ Just (Aeson.toJSON errMsg)
             & Aeson._Object . at "advice" .~ Just (Aeson.toJSON advice)
             & Aeson._Object . at "connectionType" .~ Nothing
      Nothing ->
        json & Aeson._Object . at "successful" .~ Just (Aeson.Bool True)
             & Aeson._Object . at "advice" .~ Just (Aeson.toJSON advice)
             & Aeson._Object . at "connectionType" .~ Nothing

  toJSON (RespPublish req err advice) =
    let json = _requestJSON req
    in case err of
      Just errMsg ->
        json & Aeson._Object . at "successful" .~ Just (Aeson.Bool False)
             & Aeson._Object . at "error" .~ Just (Aeson.toJSON errMsg)
             & Aeson._Object . at "advice" .~ Just (Aeson.toJSON advice)
             & Aeson._Object . at "data" .~ Nothing
      Nothing ->
        json & Aeson._Object . at "successful" .~ Just (Aeson.Bool True)
             & Aeson._Object . at "advice" .~ Just (Aeson.toJSON advice)
             & Aeson._Object . at "data" .~ Nothing

  toJSON (RespRepublish req _) = toJSON req

  toJSON resp =
    let req = _responseRequest resp
        json = _requestJSON req
    in case _responseError resp of
      Just errMsg ->
        json & Aeson._Object . at "successful" .~ Just (Aeson.Bool False)
             & Aeson._Object . at "advice" .~ Just (Aeson.toJSON $ _responseAdvice resp)
             & Aeson._Object . at "error" .~ Just (Aeson.toJSON errMsg)
      Nothing ->
        json & Aeson._Object . at "successful" .~ Just (Aeson.Bool True)
             & Aeson._Object . at "advice" .~ Just (Aeson.toJSON $ _responseAdvice resp)

instance Aeson.ToJSON a => Show (Response a) where
  show resp = Text.unpack
            . Text.decodeUtf8
            . B8.concat
            . LB8.toChunks
            $ Aeson.encodePretty resp

--------------------------------------------------------------------------------

type JSONRequest = Request Aeson.Value
type JSONResponse = Response Aeson.Value
type GenericRequest = Request GenericPayload

type RequestSubject    = Subject IO GenericRequest
type RequestObservable = Observable IO GenericRequest

type ResponseSubject = Subject IO JSONResponse
type ResponseObservable = Observable IO JSONResponse

type ClientSubject = ResponseSubject
type ClientObservable = ResponseObservable

--------------------------------------------------------------------------------

data ClientInfo
  = ClientInfo {
    _clientInfoConnectionType :: ConnectionType
  , _clientInfoSubject        :: ClientSubject
  , _clientInfoObservable     :: ClientObservable
  , _clientInfoLastSeen       :: UTCTime
  , _clientInfoSubscriptions  :: Set ChannelPattern
  , _clientInfoDisposable     :: Disposable.Disposable
  }

$(makeLenses ''ClientInfo)

data ClientStatus
  = Handshaking UTCTime
  | Connected {
      clientStatusClientInfo :: ClientInfo
  }

instance Show ClientStatus where
  show (Handshaking {}) = "Handshaking"
  show (Connected {}) = "Connected"

instance Eq ClientStatus where
  (Handshaking t1) == (Handshaking t2) = t1 == t2
  Connected {} == Connected {} = True
  _ == _ = False

instance Aeson.ToJSON ClientStatus where
  toJSON (Handshaking {}) = toJSON ("handshaking" :: String)
  toJSON (Connected clientInfo) =
    toJSON ["connected", show $ _clientInfoConnectionType clientInfo]

clientLastSeenTimestamp :: Lens' ClientStatus UTCTime
clientLastSeenTimestamp = lens getter setter
  where
    getter (Handshaking ts) = ts
    getter (Connected clientInfo) = clientInfo ^. clientInfoLastSeen

    setter (Handshaking {}) ts = Handshaking ts
    setter (Connected clientInfo) ts = Connected $ clientInfo & clientInfoLastSeen .~ ts

_Handshaking :: Prism' ClientStatus UTCTime
_Handshaking = prism' Handshaking aHandshake
  where
    aHandshake (Handshaking time) = Just time
    aHandshake _ = Nothing

_Connected :: Prism' ClientStatus ClientInfo
_Connected = prism' Connected aConnected
  where
    aConnected (Connected clientInfo) = Just clientInfo
    aConnected _ = Nothing

--------------------------------------------------------------------------------


type ClientStatusMap = HashMap ClientId ClientStatus
type ClientSubscriptionMap =
  HashMap ChannelPattern [(ClientId, Maybe (GenericPubSubMsg -> IO ()))]

data StorageState
  = StorageState {
    _storageClientStatusMap     :: ClientStatusMap
  , _storageClientSubscriptions :: ClientSubscriptionMap
  }

$(makeLenses ''StorageState)

instance Show StorageState where
  show (StorageState statusMap _) = show statusMap

instance Aeson.ToJSON StorageState where
  toJSON (StorageState clientStatusMap clientSubscriptions) =
      Aeson.object [ "statusMap" .= toJSON clientStatusMap
                   , "subscriptions" .= toJSON subscriptions ]
    where
      subscriptions = HashMap.foldrWithKey serializeSubscriptions [] clientSubscriptions
      serializeSubscriptions channelPattern subscribers acc =
        (fromChannelPattern channelPattern
        , (map serializeSubscriber subscribers, length subscribers)) : acc
      serializeSubscriber (clientId, _) = clientId

--------------------

data BayeuxTrace
  = BayeuxTrace {
    _traceRequest  :: GenericRequest
  , _traceResponse :: JSONResponse
  , _traceBefore   :: StorageState
  , _traceAfter    :: StorageState
  }
  deriving (Show, Typeable)

instance ToLogMsg BayeuxTrace where
  toLogMsg (BayeuxTrace req resp st st') =
      LText.fromChunks [ "\n"
                       , "[req=", toText req, "]\n"
                       , "[resp=", toText resp, "]\n"
                       , "[beforeState=", toText st, "]\n"
                       , "[afterState=", toText st', "]"
                       ]
    where
      toText :: Aeson.ToJSON a => a -> Text
      toText =
        Text.decodeUtf8
          . B8.concat
          . LB8.toChunks
          . Aeson.encode

instance Aeson.ToJSON BayeuxTrace where
  toJSON (BayeuxTrace req resp beforeSt afterSt) =
    Aeson.object [
        "request"  .= toJSON req
      , "response" .= toJSON resp
      , "before"   .= toJSON beforeSt
      , "after"    .= toJSON afterSt
      ]

--------------------

type TraceSubject = Subject IO BayeuxTrace
type TraceObservable = Observable IO BayeuxTrace

data Storage =
  Storage {
    _storageRequestSubject          :: RequestSubject
  , _storageResponseObservable      :: ResponseObservable
  , _storageStateObservable         :: Observable IO StorageState
  , _storageLogger                  :: Logger
  , _storageKeepAliveSubject        :: Subject IO ClientId
  , _storageGetStorageStateSnapshot :: IO StorageState
  , _storageCleanup                 :: IO ()
  }

$(makeLenses ''Storage)

class ToStorage s where
  toStorage :: s -> Storage

instance ToStorage Storage where
  toStorage = id

instance HasLogger Storage where
  getLogger = _storageLogger

data PubSub
  = PubSub {
    _pubsubClientId :: ClientId
  , _pubsubStorage  :: Storage
  , _pubsubCleanup  :: IO ()
  }

$(makeLenses ''PubSub)

instance ToStorage PubSub where
  toStorage = _pubsubStorage

instance HasLogger PubSub where
  getLogger = getLogger . _pubsubStorage

--------------------------------------------------------------------------------

data BayeuxTransportOptions =
  BayeuxTransportOptions {
    _bayeuxEndpoint           :: Text
  , _bayeuxLongPollingTimeout :: Tiempo.TimeInterval
  , _bayeuxCookieName         :: ByteString
  , _bayeuxMinifyJS           :: Bool
  }
  deriving (Show, Eq)

$(makeLenses ''BayeuxTransportOptions)
