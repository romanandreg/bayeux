module Bayeux.Util where

import Control.Applicative ((<$>))
import Data.Char (toLower)
import Data.Text (Text, pack)
import qualified Data.UUID       as UUID
import qualified Data.UUID.V4    as UUID

lowerFirstChar :: String -> String
lowerFirstChar [] = []
lowerFirstChar (c:cs) = toLower c : cs

generateId :: IO Text
generateId = pack . UUID.toString <$> UUID.nextRandom
