module Bayeux.Logger
       ( module Bayeux.Logger
       , module Rx.Logger )
       where

import           Rx.Logger (Settings (..), setupTracer)
import qualified Rx.Logger as Logger

defaultSettings :: Settings
defaultSettings = Logger.defaultSettings { envPrefix = "BAYEUX" }
