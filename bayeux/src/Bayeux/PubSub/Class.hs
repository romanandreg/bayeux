{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE MultiParamTypeClasses #-}
module Bayeux.PubSub.Class where

import Data.Aeson    (FromJSON, ToJSON)
import Data.Typeable (Typeable)

import qualified Rx.Disposable as Disposable
import           Rx.Observable (Observable (..), onNext)

import Bayeux.Types

class IPubSub m ps where
  subscribe :: (FromJSON a, ToJSON a, Typeable a)
            => ps -> ChannelPattern -> (PubSubMsg a -> m ()) -> m ()
  unsubscribe :: ps -> ChannelPattern -> m ()
  publish :: (FromJSON a, ToJSON a, Typeable a)
          => ps
          -> ChannelName
          -> a
          -> m ()


subscribeObservable :: ( FromJSON a, ToJSON a, Typeable a
                       , IPubSub IO ps )
                    => ps
                    -> ChannelPattern
                    -> Observable IO (PubSubMsg a)
subscribeObservable pubsub chanPattern =
  Observable $ \observer -> do
    subscribe pubsub chanPattern (onNext observer)
    Disposable.createDisposable $
      unsubscribe pubsub chanPattern
