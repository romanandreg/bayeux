{-# OPTIONS_GHC -fno-warn-orphans #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE ScopedTypeVariables   #-}
module Bayeux.PubSub.IO
       ( module Bayeux.PubSub.IO
       , PubSub(..)
       , IPubSub(..)
       , PubSubMsg (..)
       , GenericPayload ) where


import           Control.Applicative ((<$>))
import           Data.Aeson          (FromJSON, Result (..), ToJSON, Value (..),
                                      fromJSON, object, (.=))
import           Data.Text           (Text, pack)
import           Data.Typeable       (Typeable, cast)
import qualified Data.UUID           as UUID
import qualified Data.UUID.V4        as UUID

import Bayeux.PubSub.Class
import Bayeux.Storage.Core (handleRequest, processHandshake)
import Bayeux.Types

--------------------------------------------------------------------------------

toGenericResponse :: (FromJSON a, ToJSON a, Typeable a)
                  => Response a
                  -> Response GenericPayload
toGenericResponse resp = GenericPayload <$> resp

fromGenericHandler :: (FromJSON a, ToJSON a, Typeable a)
                   => (PubSubMsg a -> IO ())
                   -> GenericPubSubMsg
                   -> IO ()
fromGenericHandler handler (PubSubMsg clientId chan msgid (GenericPayload raw)) =
  case cast raw of
    Just val -> handler (PubSubMsg clientId chan msgid val)
    Nothing  ->
      case cast raw of
        Just (jsonVal :: Value) ->
          case fromJSON jsonVal of
            Success val -> handler (PubSubMsg clientId chan msgid val)
            Error _ -> return ()
        Nothing -> return ()


--------------------------------------------------------------------------------

newPubSubWithStorage :: Storage -> IO PubSub
newPubSubWithStorage storage = do
  handshakeMsgId <- pack . UUID.toString <$> UUID.nextRandom
  let handshakeReq =
        ReqHandshake (object [ "channel" .= (String "/meta/handshake")
                             , "id"      .= handshakeMsgId ])
                     (Just handshakeMsgId)
                     Nothing
  handshakeResp <- processHandshake storage handshakeReq
  case handshakeResp of
    Just (RespHandshake _ (Right clientId) _) -> do
      connectMsgId <- pack . UUID.toString <$> UUID.nextRandom
      let connectReq = ReqConnect (object [ "channel" .= (String "/meta/connect")
                                          , "id" .= connectMsgId
                                          , "clientId" .= clientId ])
                                  (Just connectMsgId)
                                  clientId
                                  InProcess
      handleRequest storage (connectReq :: Request ())
      return PubSub {  _pubsubClientId = clientId
                    ,  _pubsubStorage  = storage
                    ,  _pubsubCleanup  = _storageCleanup storage }

    Just (RespHandshake _ (Left errMsg) _) ->
      error (show errMsg)
    _ ->
      error "Bayeux.PubSub.IO: can't add a new client"


--------------------------------------------------------------------------------

instance IPubSub IO PubSub where
  subscribe pubsub chanPattern handler = do
    msgid <- pack . UUID.toString <$> UUID.nextRandom
    let clientId = _pubsubClientId pubsub
        req = ReqSubscribe (object [ "channel" .= ("/meta/subscribe" :: Text)
                                   , "id" .= msgid
                                   , "clientId" .= clientId
                                   , "subscription" .= chanPattern ])
                           (Just msgid)
                           clientId
                           chanPattern
                           (Just $ fromGenericHandler handler)
    handleRequest pubsub (req :: Request ())

  unsubscribe pubsub chanPattern = do
    msgid <- pack . UUID.toString <$> UUID.nextRandom
    let clientId = _pubsubClientId pubsub
        req = ReqUnsubscribe (object [ "channel" .= ("/meta/unsubscribe" :: Text)
                                     , "id" .= msgid
                                     , "clientId" .= clientId
                                     , "subscription" .= chanPattern ])
                             (Just msgid)
                             clientId
                             chanPattern
    handleRequest pubsub (req :: Request ())

  publish pubsub chanName payload = do
    msgid <- pack . UUID.toString <$> UUID.nextRandom
    let clientId = _pubsubClientId pubsub
        req = ReqPublish (object [ "channel" .= ("/meta/publish" :: Text)
                                               , "id" .= msgid
                                               , "clientId" .= clientId
                                               , "channel" .= chanName ])
                                       (Just msgid)
                                       clientId
                                       chanName
                                       payload
    handleRequest pubsub req
