{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE OverloadedStrings  #-}
{-# LANGUAGE TemplateHaskell    #-}
module Bayeux.PubSub.RPC where

import           Control.Applicative     ((<$>))
import qualified Control.Concurrent.MVar as MVar
import           Control.Exception       (Exception)
import           Control.Monad           (when)

import Data.Monoid   (mconcat)
import Data.Text     (Text)
import Data.Typeable (Typeable)

import           Data.Aeson       ((.:), (.=))
import qualified Data.Aeson       as Aeson
import qualified Data.Aeson.TH    as Aeson
import           Data.Aeson.Types (typeMismatch)

import qualified Tiempo            as Tiempo
import qualified Tiempo.Concurrent as Tiempo

import qualified Rx.Logger as Logger

import Bayeux.PubSub
import Bayeux.Types
import Bayeux.Util   (generateId, lowerFirstChar)

--------------------------------------------------------------------------------

data RPCRequest payload
  = RPCRequest {
    _rpcRequestMsgId        :: MsgId
  , _rpcRequestReplyChannel :: ChannelName
  , _rpcRequestPayload      :: payload
  }
  deriving (Typeable)

$(Aeson.deriveJSON
    (Aeson.defaultOptions {
        Aeson.fieldLabelModifier = (lowerFirstChar . drop 11) })
    ''RPCRequest)

data RPCResponse payload
  = RPCResponse {
    _rpcResponseMsgId   :: MsgId
  , _rpcResponsePayload :: Either Text payload
  }
  deriving (Typeable)

instance Aeson.FromJSON a => Aeson.FromJSON (RPCResponse a) where
  parseJSON (Aeson.Object obj) = do
    msgId <- obj .: "msgId"
    successful <- obj .: "successful"
    if successful
       then do
         payload <- obj .: "payload"
         return $ RPCResponse msgId $ Right payload
       else do
         payload <- obj .: "payload"
         return $ RPCResponse msgId $ Left payload
  parseJSON json = typeMismatch "RPCResponse" json

instance Aeson.ToJSON a => Aeson.ToJSON (RPCResponse a) where
  toJSON (RPCResponse msgId (Left payload)) =
    Aeson.object [ "msgId" .= msgId
                 , "successful" .= False
                 , "payload" .= payload ]

  toJSON (RPCResponse msgId (Right payload)) =
    Aeson.object [ "msgId" .= msgId
                 , "successful" .= True
                 , "payload" .= payload ]

--------------------

data RPCError
  = RPCTimeout
  | RPCCallError Text
  deriving (Show, Eq, Typeable)

instance Exception RPCError
$(Aeson.deriveJSON Aeson.defaultOptions ''RPCError)

--------------------------------------------------------------------------------

_rpcCall
  :: ( Aeson.FromJSON payload, Aeson.ToJSON payload, Typeable payload
     , Aeson.FromJSON result, Aeson.ToJSON result, Typeable result )
  => Maybe Tiempo.TimeInterval
  -> PubSub
  -> ChannelName
  -> payload
  -> IO (Either RPCError result)
_rpcCall mInterval pubsub chan payload = do
  let clientId = _pubsubClientId pubsub
      replyChanRaw = mconcat ["/", clientId, "/rpc", fromChannelName chan]
      Right replyChanName =
        normalizeChannelName replyChanRaw
      Right replyChanPattern =
        normalizeChannelPattern replyChanRaw

  msgId <- generateId
  resultVar <- MVar.newEmptyMVar
  subscribe pubsub replyChanPattern $ \msg -> do
    let result = _msgPayload msg
    when (_rpcResponseMsgId result == msgId) $ do
      unsubscribe pubsub replyChanPattern
      MVar.putMVar resultVar $ _rpcResponsePayload result

  Logger.scope pubsub '_rpcCall $
    Logger.finest $
      mconcat ["rpcCall in progress "
              , "[msgId=", msgId, "]"
              , "[replyChan=", replyChanRaw , "]"
              ]

  let req = RPCRequest msgId replyChanName payload
  publish pubsub chan req

  -- NOTE: Make this a configuration setting
  result <- case mInterval of
    Just interval -> Tiempo.timeout interval
                                    (MVar.takeMVar resultVar)
    Nothing -> Just <$> MVar.takeMVar resultVar

  case result of
    Just (Right result1) -> do
      Logger.scope pubsub '_rpcCall $
        Logger.finest $
          mconcat [ "rpcCall reply successful "
                  , "[msgId=", msgId, "]"
                  ]
      return $! Right result1
    Just (Left err) -> do
      Logger.scope pubsub '_rpcCall $
        Logger.finest $
          mconcat [ "rpcCall reply with error "
                  , "[msgId=", msgId, "]"
                  , "[errMsg=", err, "]"
                  ]
      return $! Left $ RPCCallError err
    Nothing -> do
      Logger.scope pubsub '_rpcCall $
        Logger.finest $
          mconcat [" rpcCall timedout "
                  , "[msgId=", msgId, "]"
                  ]
      return $! Left RPCTimeout

rpcCallTimeout
  :: ( Aeson.FromJSON payload, Aeson.ToJSON payload, Typeable payload
     , Aeson.FromJSON result, Aeson.ToJSON result, Typeable result )
  => Tiempo.TimeInterval
  -> PubSub
  -> ChannelName
  -> payload
  -> IO (Either RPCError result)
rpcCallTimeout interval = _rpcCall (Just interval)

rpcCall :: ( Aeson.FromJSON payload, Aeson.ToJSON payload, Typeable payload
           , Aeson.FromJSON result, Aeson.ToJSON result, Typeable result )
        => PubSub
        -> ChannelName
        -> payload
        -> IO (Either RPCError result)
rpcCall = _rpcCall Nothing

rpcEndpoint :: ( Aeson.FromJSON payload, Aeson.ToJSON payload, Typeable payload
               , Aeson.FromJSON result, Aeson.ToJSON result, Typeable result )
            => PubSub
            -> ChannelPattern
            -> (payload -> IO (Either Text result))
            -> IO ()
rpcEndpoint pubsub chan callback =
  subscribe pubsub chan $ \msg  -> do
    let (RPCRequest msgId replyChanPattern payload) = _msgPayload msg
    let finest txt =
            Logger.scope pubsub 'rpcEndpoint $
              Logger.finest $ mconcat [txt, " [msgId:", msgId ,"]"]

    finest "rpcEndpoint received request"
    result <- callback payload
    finest "rpcEndpoind sending response"
    publish pubsub replyChanPattern (RPCResponse msgId result)

--------------------------------------------------------------------------------
