{-# LANGUAGE OverloadedStrings #-}
module Bayeux.ChannelSpec where

import Data.Text (Text)
import qualified Data.Text as Text

import Bayeux.Types

normalizeChan :: Text -> Either String Text
normalizeChan chan
  | Text.null chan = Right $ Text.pack "/"
  | Text.isInfixOf (Text.pack "//") chan =
    Left $ "Invalid channel name: " ++ Text.unpack chan
  | not $ Text.isPrefixOf (Text.pack "/") chan =
    normalizeChan $ Text.concat ["/", chan]
  | Text.isSuffixOf (Text.pack "/") chan = Right $ Text.init chan
  | otherwise = Right chan

normalizeChannelName :: Text -> Either String ChannelName
normalizeChannelName input = fmap ChannelName (normalizeChan input)

normalizeChannelPattern :: Text -> Either String ChannelPattern
normalizeChannelPattern input = fmap ChannelPattern (normalizeChan input)

matchingChan :: ChannelName -> ChannelPattern -> Bool
matchingChan (ChannelName chanName) (ChannelPattern chanPattern) =
    compChans chanName' chanPattern'
  where
    chanName'    = Text.splitOn "/" chanName
    chanPattern' = Text.splitOn "/" chanPattern
    compChans _ ("**":_)    = True
    compChans [_] ["*"]     = True
    compChans [] ["*"]      = True
    compChans (x:xs) (y:ys) = x == y && compChans xs ys
    compChans xs ys         = xs == ys
