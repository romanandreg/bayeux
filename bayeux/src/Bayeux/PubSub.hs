module Bayeux.PubSub
       ( module Bayeux.PubSub.Class
       , newPubSub
       , newPubSubWithStorage
       , disconnectClient

       , PubSub
       , PubSubMsg (..)
       , StorageState (..)
       , ClientId
       , ChannelName
       , ChannelPattern

       , getStateSnapshot
       , storageClientStatusMap
       , storageClientSubscriptions
       , clientConnectObservable
       , clientDisconnectObservable
       , normalizeChannelName
       , normalizeChannelPattern

       , GenericRequest
       , JSONResponse
       , Request
       , Response
       , BayeuxTrace
       ) where

import Bayeux.ChannelSpec
import Bayeux.PubSub.Class
import Bayeux.PubSub.IO
import Bayeux.Storage.Core
import Bayeux.Storage.Memory (memoryStorage)
import Bayeux.Types

newPubSub :: IO PubSub
newPubSub = do
  storage <- memoryStorage
  newPubSubWithStorage storage
