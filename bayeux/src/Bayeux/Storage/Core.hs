{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}
module Bayeux.Storage.Core where

import Control.Applicative
import Control.Lens        hiding ((.=))

import Control.Concurrent       (threadDelay)
import Control.Concurrent.Async (async, wait)

import Control.Monad       (foldM)
import Control.Monad.Trans (MonadIO (..))

import Data.Aeson    (FromJSON, ToJSON, Value, object, (.=))
import Data.Maybe    (isJust)
import Data.Text     (pack)
import Data.Time     (diffUTCTime, getCurrentTime)
import Data.Typeable (Typeable)

import qualified Data.UUID    as UUID
import qualified Data.UUID.V4 as UUID

import qualified Data.HashMap.Strict as HashMap
import qualified Data.Set            as Set

import qualified Tiempo as Tiempo

import           Rx.Logger     (MonadLog (..))
import qualified Rx.Logger     as Logger
import           Rx.Observable (Observable)
import qualified Rx.Observable as Observable
import           Rx.Subject    (onNext)

import Bayeux.Response (isConnectResponse, isDisconnectResponse,
                        isValidResponse)
import Bayeux.Types

--------------------------------------------------------------------------------


disconnectClient :: ToStorage s => s -> ClientId -> IO ()
disconnectClient storage clientId = do
  msgId <- pack . UUID.toString <$> UUID.nextRandom
  let req = ReqDisconnect (object [ "id" .= msgId
                                  , "clientId" .= clientId])
                          (Just msgId)
                          clientId
  handleRequest storage (req :: Request ())

handleRequest :: (Typeable a, FromJSON a, ToJSON a, ToStorage s)
              => s -> Request a -> IO ()
handleRequest storage req =
  onNext (_storageRequestSubject $ toStorage storage)
         (GenericPayload `fmap` req)

processHandshake
  :: ToStorage s => s -> Request Value -> IO (Maybe (Response Value))
processHandshake storage req = do
    let handshakeMsgId = req ^. requestMsgId
    result <- async $ getHandshakeClientId handshakeMsgId
    threadDelay 1000
    handleRequest (toStorage storage) req
    wait result
  where
    getHandshakeClientId Nothing = return Nothing
    getHandshakeClientId (Just msgId) = do
      let handshakeObs = Observable.timeout (Tiempo.milliSeconds 1500)
                         $ Observable.concatMap (isHandshake msgId)
                         $ _storageResponseObservable (toStorage storage)
      Observable.toMaybe handshakeObs
    isHandshake msgId resp@(RespHandshake {})
      | resp ^. responseRequest . requestMsgId == Just msgId = [resp]
      | otherwise = []
    isHandshake _ _ = []

keepClientAlive :: ToStorage s => s -> ClientId -> IO ()
keepClientAlive storage clientId =
  onNext (_storageKeepAliveSubject $ toStorage storage) clientId

clientConnectObservable :: ToStorage s => s -> Observable IO ClientId
clientConnectObservable storage =
   Observable.distinct .
     Observable.map (^. responseRequest . requestClientId) .
     Observable.filter (\r -> isValidResponse r && isConnectResponse r) $
     _storageResponseObservable $
     toStorage storage

clientDisconnectObservable :: ToStorage s => s -> Observable IO ClientId
clientDisconnectObservable storage =
   Observable.distinct .
     Observable.map (^. responseRequest . requestClientId) .
     Observable.filter (\r -> isValidResponse r && isDisconnectResponse r) $
     _storageResponseObservable $
     toStorage storage

getClientCount :: ToStorage s => s -> IO Int
getClientCount storage = do
  state <- _storageGetStorageStateSnapshot $ toStorage storage
  return $ state ^. storageClientStatusMap . to HashMap.size

getStateSnapshot :: ToStorage s => s -> IO StorageState
getStateSnapshot =
  _storageGetStorageStateSnapshot . toStorage

getClientSubject :: ToStorage s => s -> ClientId -> IO (Maybe ClientSubject)
getClientSubject storage clientId = do
  state <- _storageGetStorageStateSnapshot $ toStorage storage
  case state ^. storageClientStatusMap . at clientId of
    Just (Connected clientInfo) ->
      return $ Just (_clientInfoSubject clientInfo)
    _ -> return Nothing

getClientObservable :: ToStorage s => s -> ClientId -> IO (Maybe ClientObservable)
getClientObservable storage clientId = do
  state <- _storageGetStorageStateSnapshot $ toStorage storage
  case state ^. storageClientStatusMap . at clientId of
    Just (Connected clientInfo) ->
      return $ Just (_clientInfoObservable clientInfo)
    _ -> return Nothing

clientErrorObservable
  :: ToStorage s => s -> ClientId -> ClientObservable
clientErrorObservable storage0 clientId =
    Observable.filter responseWithError
      . _storageResponseObservable
      $ toStorage storage0
  where
    responseWithError (RespHandshake _ (Left _) _) = True
    responseWithError resp =
      (resp ^. responseClientId == clientId) &&
      (isJust $ _responseError resp)

getTraceObservable
  :: ToStorage s
  => s
  -> Observable IO BayeuxTrace
getTraceObservable storage =
  Observable.cast (Logger.fromLogMsg . Logger._logEntryMsg)
    . Observable.toObservable
    . Logger.getLogger
    $ toStorage storage

getRequestObservable
  :: ToStorage s
  => s
  -> Observable IO GenericRequest
getRequestObservable storage =
  Observable.map _traceRequest $ getTraceObservable storage

--------------------------------------------------------------------------------
-- Private APIs

deleteClient :: StorageState -> ClientId -> ClientStatus -> StorageState
deleteClient st clientId (Handshaking _) = do
  st & (storageClientStatusMap . at clientId %~ const Nothing)

deleteClient st clientId (Connected clientInfo) = do
      st & (storageClientStatusMap . at clientId %~ const Nothing)
         & (storageClientSubscriptions %~ rmClientSubscriptions)
  where
    clientSubs = _clientInfoSubscriptions clientInfo
    rmClientSubscriptions :: ClientSubscriptionMap
                          -> ClientSubscriptionMap
    rmClientSubscriptions allSubsMap =
      if Set.null clientSubs
         then allSubsMap
         else
           Set.foldr'
             (\chan subsMap -> subsMap & at chan %~ rmSubscriptions)
             allSubsMap
             clientSubs

    rmSubscriptions Nothing = Nothing
    rmSubscriptions (Just subs) =
      let subs' = filter ((/= clientId) . fst) subs
      in if null subs' then Nothing else Just subs'

updateClientLastSeen :: (MonadLog m, MonadIO m)
                     => StorageState -> ClientId -> m StorageState
updateClientLastSeen state0 clientId = do
  Logger.traceFormat
    "[clientId={}] Update client's lastSeen timestamp"
    (Logger.Only clientId)
  now <- liftIO getCurrentTime
  return $ state0 & storageClientStatusMap
                  . at clientId . _Just . _Connected . clientInfoLastSeen
                  .~ now

cleanupIdleClients
  :: (MonadLog m, MonadIO m)
  => (ClientStatus -> Bool)
  -> StorageState
  -> m StorageState
cleanupIdleClients predFn state0 = do
    Logger.info ("Start cleaning idle clients" :: String)
    now <- liftIO getCurrentTime
    let clientIdList = HashMap.toList $ _storageClientStatusMap state0
    state <- foldM (cleanupClient now) state0 clientIdList
    Logger.info ("Finished cleaning idle clients" :: String)
    return state
  where
    getLastSeen (Handshaking timestamp) = timestamp
    getLastSeen (Connected clientInfo) = _clientInfoLastSeen clientInfo

    cleanupClient now state (clientId, clientStatus) = do
      let lastSeen   = getLastSeen clientStatus
          difference = diffUTCTime lastSeen now
          -- TODO: create a BayeuxStorageOptions type, and take timeout
          -- out of there
          maxDifference = Tiempo.toNominalDiffTime $ Tiempo.minutes 1

      if difference > maxDifference && predFn clientStatus
        then do
          Logger.infoFormat
            "[clientId={}] Removed idle client from Storage"
            (Logger.Only clientId)
          return $ deleteClient state clientId clientStatus
        else return state
