{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell     #-}
module Bayeux.Storage.Memory (
    Storage (..)
  , memoryStorage
  , handleRequest
  , getClientSubject
  , getClientObservable
  , storageResponseObservable
  ) where

import Control.Lens hiding ((.=))

import Control.Applicative ((<$>))
import Control.Exception   (SomeException, try)
import Control.Monad       (forM_, when)
import Control.Monad.Trans (MonadIO (..))

import           Control.Concurrent.STM      (atomically)
import           Control.Concurrent.STM.TVar (TVar)
import qualified Control.Concurrent.STM.TVar as TVar

import qualified Data.Aeson as Aeson

import qualified Data.HashMap.Strict as HashMap
import qualified Data.Set            as Set
import qualified Data.Text           as Text
import           Data.Time           (getCurrentTime)
import qualified Data.UUID           as UUID
import qualified Data.UUID.V4        as UUID

import qualified Tiempo as Tiempo

import qualified Rx.Disposable as Disposable
import           Rx.Logger     (MonadLog)
import qualified Rx.Logger     as Logger
import qualified Rx.Observable as Observable
import           Rx.Subject    (onNext, publishSubject, singleSubject,
                                singleSubjectWithQueue, toObservable)

import Bayeux.ChannelSpec  (matchingChan)
import Bayeux.Logger       (defaultSettings, setupTracer)
import Bayeux.Storage.Core (cleanupIdleClients, deleteClient,
                            getClientObservable, getClientSubject,
                            handleRequest, updateClientLastSeen)
import Bayeux.Types

--------------------------------------------------------------------------------

data StateMachineReq
  = BayeuxRequest GenericRequest
  | KeepAlive ClientId
  | CleanupIdle

memoryStorage :: IO Storage
memoryStorage =
  _memoryStorage $ StorageState HashMap.empty HashMap.empty

_memoryStorage :: StorageState -> IO Storage
_memoryStorage st0 = do
  reqSubject       <- publishSubject
  respSubject      <- publishSubject
  keepAliveSubject <- publishSubject
  logger           <- Logger.newLogger

  stateVar <- TVar.newTVarIO st0

  let cleanupIntervalMinutes = 1 :: Int
  let reqObservable =
        Observable.mergeList [
          Observable.map BayeuxRequest $ toObservable reqSubject
        , Observable.map KeepAlive $ toObservable keepAliveSubject
        , Observable.map (\(_ :: Int) -> CleanupIdle)
            $ Observable.interval (Tiempo.minutes cleanupIntervalMinutes)
                                  (Tiempo.minutes cleanupIntervalMinutes)
        ]
      respObservable =
        Observable.map memoryResponseDecorator
          $ toObservable respSubject
      stateObservable =
        Observable.scanM
          (\st req ->
            Logger.scope logger '_memoryStorageStateMachine $
              _memoryStorageStateMachine respSubject respObservable st req)
          st0
          reqObservable

  bayeuxDisp <- Disposable.newCompositeDisposable

  stateReductionDisp <-
      Observable.subscribe
         stateObservable
         (atomically . TVar.writeTVar stateVar)
         (\err -> Logger.scope logger '_memoryStorage $
             Logger.severe $ "Failure on Bayeux state machine: " ++ show err)
         (return ())

  Disposable.append stateReductionDisp bayeuxDisp
  setupTracer defaultSettings logger >>= flip Disposable.append bayeuxDisp


  return $ Storage reqSubject
                   respObservable
                   stateObservable
                   logger
                   keepAliveSubject
                   (memoryGetStorageStateSnapshot stateVar)
                   (Disposable.dispose stateReductionDisp)


_memoryStorageStateMachine
  :: (MonadLog m, MonadIO m)
  => ResponseSubject
  -> ResponseObservable
  -> StorageState
  -> StateMachineReq
  -> m StorageState
_memoryStorageStateMachine respSubject respObservable
                           st (BayeuxRequest req) = do
  (resp, st') <- memoryHandleRequest respObservable st req
  let jsonResp = Aeson.toJSON <$> resp
  liftIO $ onNext respSubject jsonResp
  Logger.finest $ BayeuxTrace req jsonResp st st'
  return st'

_memoryStorageStateMachine _ _ st (KeepAlive clientId) =
  updateClientLastSeen st clientId

_memoryStorageStateMachine _ _ st CleanupIdle =
    cleanupIdleClients shouldCleanupClient st
  where
    shouldCleanupClient (Handshaking _) = True
    shouldCleanupClient (Connected clientInfo) =
      case _clientInfoConnectionType clientInfo of
        LongPolling -> True
        CallbackPolling _ -> True
        _ -> False

--------------------------------------------------------------------------------

memoryResponseDecorator :: Response a -> Response a
memoryResponseDecorator resp@(RespHandshake {}) = resp
memoryResponseDecorator resp =
  case _responseError resp of
    -- When having an unknown clientId error, add a Handshake
    Just UnknownClientId  ->
      resp & responseAdvice . adviceReconnect .~ Just Handshake
    _ ->
      resp & responseAdvice . adviceReconnect .~ Just Retry

--------------------------------------------------------------------------------

memoryHandleRequest :: (MonadLog m, MonadIO m)
                    => ResponseObservable
                    -> StorageState
                    -> Request GenericPayload
                    -> m (Response GenericPayload, StorageState)
memoryHandleRequest _ state req@(ReqHandshake {}) = do
    now <- liftIO getCurrentTime
    clientId <- liftIO $ (Text.pack . UUID.toString) <$> UUID.nextRandom
    Logger.traceFormat
      "[clientId={}] Processing handshake request" (Logger.Only clientId)
    let state' = state & storageClientStatusMap . at clientId .~ Just (Handshaking now)
    return (RespHandshake req (Right clientId) emptyAdvice, state')

memoryHandleRequest respObservable state req@(ReqConnect  _ _ clientId connType) =
    case state ^. storageClientStatusMap . at clientId of
      Just (Handshaking {}) -> do
        Logger.traceFormat
          "[clientId={}][connType={}] Processing connect request"
          (clientId, show connType)
        clientInfo <- liftIO setupClientInfo
        let state' = state & storageClientStatusMap . at clientId
                           .~ Just (Connected clientInfo)
        return (RespConnect req Nothing emptyAdvice, state')
      Just _ -> do
        Logger.traceFormat
          "[clientId={}][connType={}] Processing connect request (already connected)"
          (clientId, show connType)
        return (RespConnect req Nothing emptyAdvice, state)
      _ -> do
        Logger.traceFormat
          "[clientId={}][connType={}] Processing connect request (unknown client)"
          (clientId, show connType)
        return (RespConnect req (Just UnknownClientId) emptyAdvice, state)
  where
    isClientResponsesOrRepublish (RespRepublish {}) = True
    isClientResponsesOrRepublish response =
      response ^. responseClientId == clientId

    setupClientInfo = do
      clientSubject <- if isPollingConnectionType connType
                          -- In case of Polling requests, we want
                          -- to have a queue to store events that
                          -- happened while there wasn't any subscribers
                          -- in between requests
                          then singleSubjectWithQueue
                          -- other connectionType means it is
                          -- not going to be used in Polling requests,
                          -- so no need to queue them up
                          -- (Reduces memory footprint)
                          else singleSubject

      clientDisposable <- Disposable.newSingleAssignmentDisposable
      clientLastSeen <- getCurrentTime
      let clientSubscriptions = Set.empty

      let clientRespObservable =
            Observable.filter isClientResponsesOrRepublish
              $ respObservable
          clientObservable = toObservable clientSubject

      innerDisposable <-
        Observable.subscribe
          clientRespObservable
          (onNext clientSubject)
          (\err -> do
              Disposable.dispose clientDisposable
              Observable.onError clientSubject err)
          (do Disposable.dispose clientDisposable
              Observable.onCompleted clientSubject)

      Disposable.set innerDisposable clientDisposable
      return $ ClientInfo connType
                          clientSubject clientObservable
                          clientLastSeen clientSubscriptions
                          $ Disposable.toDisposable clientDisposable

memoryHandleRequest _ state req@(ReqDisconnect _ _ clientId) =
  case state ^. storageClientStatusMap . at clientId of
    Just clientStatus@(Connected {}) -> do
      Logger.traceFormat
        "[clientId={}] Processing disconnect request" (Logger.Only clientId)
      Logger.traceFormat
        "[clientId={}] Remove client from Storage" (Logger.Only clientId)

      let state' = deleteClient state clientId clientStatus
      return (RespDisconnect req Nothing emptyAdvice, state')
    _ -> do
      Logger.traceFormat
        "[clientId={}] Processing disconnect request (unknown client)"
        (Logger.Only clientId)
      return (RespDisconnect req (Just UnknownClientId) emptyAdvice, state)

memoryHandleRequest _ state req@(ReqSubscribe _ _ clientId chan handler) =
  case state ^. storageClientStatusMap . at clientId of
    Just (Connected _) -> do
      Logger.traceFormat
        "[clientId={}][chan={}] Processing subscribe request" (clientId, chan)
      let entry = (clientId, handler)
          state' =
            state & storageClientSubscriptions . at chan
                    %~ Just . maybe [entry] (entry:)
                  & storageClientStatusMap . at clientId
                    . _Just . _Connected . clientInfoSubscriptions
                    %~ Set.insert chan
      return (RespSubscribe req Nothing emptyAdvice, state')
    _ ->
      return (RespSubscribe req (Just UnknownClientId) emptyAdvice, state)

memoryHandleRequest _  state req@(ReqUnsubscribe _ _ clientId chan) = do
    case state ^. storageClientStatusMap . at clientId of
      Just (Connected _) -> do
        Logger.traceFormat
          "[clientId={}][chan={}] Processing unsubscribe request" (chan, clientId)
        let state' =
              state & storageClientSubscriptions . at chan
                      %~ removeEntry
                    & storageClientStatusMap . at clientId
                      . _Just . _Connected . clientInfoSubscriptions
                      %~ Set.delete chan

        return (RespSubscribe req Nothing emptyAdvice, state')
      _ -> do
        Logger.traceFormat
          "[clientId={}][chan={}] Processing unsubscribe request (unknown client)"
          (clientId, chan)
        return (RespSubscribe req (Just UnknownClientId) emptyAdvice, state)
  where
    removeEntry Nothing = Nothing
    removeEntry (Just subs) =
      case filter ((/= clientId) . fst) subs of
        [] -> Nothing
        entry -> Just entry

memoryHandleRequest _ state
                    req@(ReqPublish _ (Just msgid) clientId chanName payload) = do
  case state ^. storageClientStatusMap . at clientId of
    Just (Connected {}) -> do
      Logger.traceFormat
         "[clientId={}][msgId={}][chan={}] Processing publish request "
         (clientId, msgid, chanName)
      liftIO publishToAll
      return (RespPublish req Nothing emptyAdvice, state)
    _ -> do
      Logger.traceFormat
         "[clientId={}][msgId={}][chan={}] Processing publish request (unknown client)"
         (clientId, msgid, chanName)
      return (RespPublish req (Just UnknownClientId) emptyAdvice, state)
  where
    publishToAll =
      forM_ (HashMap.toList $ state ^. storageClientSubscriptions)
           $ \(chanPattern, subscribers) ->
             when (matchingChan chanName chanPattern) $
               forM_ subscribers $ \(clientId', mHandler) -> do
                 case mHandler of
                   Just handler -> do
                     (_ :: Either SomeException ()) <-
                         try $ handler (PubSubMsg clientId chanName msgid payload)
                     return ()
                   _ -> return ()

                 case state ^. storageClientStatusMap . at clientId' of
                   Just (Connected clientInfo) ->
                     onNext (_clientInfoSubject clientInfo)
                         $ Aeson.toJSON <$> RespRepublish req clientId
                   _ -> return ()

memoryHandleRequest _ _ (ReqPublish _ Nothing _ _ _) = do
  let msg = "Bayeux.memoryHandleRequest: can't handle publish request without a msgId"
  Logger.severe msg
  error msg

memoryGetStorageStateSnapshot :: TVar StorageState -> IO StorageState
memoryGetStorageStateSnapshot = atomically . TVar.readTVar
