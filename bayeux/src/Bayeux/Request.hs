module Bayeux.Request where

import Control.Applicative

import qualified Data.UUID           as UUID
import qualified Data.UUID.V4        as UUID
import Data.Text (pack)

import Bayeux.Types

getOrCreateMsgId :: Request a -> IO (MsgId, Request a)
getOrCreateMsgId req = do
  case _requestMsgId req of
    Just msgId -> return (msgId, req)
    Nothing -> do
      msgId <- pack . UUID.toString <$> UUID.nextRandom
      return (msgId, req { _requestMsgId = Just msgId })
