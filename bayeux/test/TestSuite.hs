module Main where

import qualified Bayeux.PubSubTest
import qualified Bayeux.RPCTest
import Test.Hspec

main :: IO ()
main = hspec $ do
  Bayeux.PubSubTest.tests
  Bayeux.RPCTest.tests
