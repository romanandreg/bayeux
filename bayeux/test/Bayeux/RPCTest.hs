{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE OverloadedStrings #-}
module Bayeux.RPCTest where

import Data.Char (toUpper)

import Bayeux.PubSub     (newPubSub)
import Bayeux.PubSub.RPC

import Test.Hspec
import Test.HUnit (assertEqual, assertFailure)

tests :: Spec
tests =
  describe "rpc" $ do

    describe "rpcCall / rpcEndpoint" $ do

      it "returns succesful response correctly" $ do
        pubsub <- newPubSub
        rpcEndpoint pubsub "/hello"
          $ return . Right . map toUpper

        eResult <- rpcCall pubsub "/hello" ("world" :: String)
        case eResult of
          Right result ->
            assertEqual "should return msg from RPC" ("WORLD" :: String) result
          _ ->
            assertFailure "should not return an error"
