{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}
module Bayeux.PubSubTest (tests) where

import Control.Lens

import           Control.Concurrent          (threadDelay)
import qualified Control.Concurrent.Async    as Async
import           Control.Concurrent.STM      (atomically)
import           Control.Concurrent.STM.TVar (modifyTVar, newTVarIO, readTVar)
import           Control.Monad               (forM_)
-- import           Control.Monad.Trans         (MonadIO (..))

import Test.Hspec
import Test.HUnit (assertBool, assertEqual, assertFailure)

import qualified Tiempo as Tiempo

import qualified Data.Aeson    as Aeson
import qualified Rx.Observable as Observable

import Bayeux.PubSub.IO
import Bayeux.Storage.Core   (getTraceObservable)
import Bayeux.Storage.Memory (handleRequest, memoryStorage)
import Bayeux.Types


-- printAsJSON :: Aeson.ToJSON a => String -> Notification a -> IO ()
-- printAsJSON prefix (OnNext v) = putStrLn $ prefix ++ " " ++ show (Aeson.encode v)
-- printAsJSON prefix (OnError err) = putStrLn $ prefix ++ " " ++ show err
-- printAsJSON prefix OnCompleted = putStrLn $ prefix ++ " DONE"

tests :: Spec
tests =
  describe "pubsub" $ do

    describe "handleRequest" $ do

      it "returns an error response when clientId not connected" $ do
        storage <- memoryStorage
        pubsub <- newPubSubWithStorage storage
        let req = ReqPublish (Aeson.object []) (Just "123") "123" "/hello" 'a'
            obs  = Observable.first $ storage ^. storageResponseObservable

        resultAsync <- Async.async $ Observable.toList obs
        threadDelay 10000
        handleRequest pubsub req
        result <- Async.wait resultAsync

        case result of
          Left _ -> assertBool "should not respond with an error" False
          Right [RespPublish (ReqPublish _ (Just msgid) _ _ _) _ _] ->
            assertEqual "req should have resp" "123" msgid
          Right rs ->
            assertFailure $ "should return a single response " ++ show (length rs)


    describe "traceObservable" $
      it "emits bayeux storage state after request is handled" $ do
        storage <- memoryStorage
        let traceObservable = Observable.timeout (Tiempo.milliSeconds 100)
                              $ getTraceObservable storage

        resultAsync <- Async.async $ Observable.toList traceObservable
        threadDelay 1000

        pubsub <- newPubSubWithStorage storage
        publish pubsub "/hello" ("world" :: String)

        result <- Async.wait resultAsync
        case result of
          Left (entries, _)
            | null entries ->
                assertFailure "should not return empty list"
            | otherwise ->
                forM_ entries $ \(BayeuxTrace req resp _ _) -> do
                  assertEqual "req and resp should have same MsgId"
                              (req ^. requestMsgId)
                              (resp ^. responseMsgId)
          _ -> assertFailure "the impossible happened"

    describe "publish" $ do

      it "json messages are processed correctly" $ do
        storage <- memoryStorage
        pubsub <- newPubSubWithStorage storage
        sumVar <- newTVarIO 0

        subscribe pubsub "/sum"
          $ \(PubSubMsg _ _ _ (n :: Int)) -> atomically $ modifyTVar sumVar (+n)

        subscribe pubsub "/*"
          $ \(PubSubMsg _ _ _ (n :: Int)) -> atomically $ modifyTVar sumVar (+n)

        publish pubsub "/sum" (Aeson.Number 1)
        n <- atomically $ readTVar sumVar
        assertEqual "number should be incremented by all subscriptions" 2 n


      it "sends messages to all clients subscribed" $ do
        storage <- memoryStorage
        pubsub <- newPubSubWithStorage storage
        sumVar <- newTVarIO 0

        subscribe pubsub "/sum"
          $ \(PubSubMsg _ _ _ (n :: Int)) -> atomically $ modifyTVar sumVar (+n)

        subscribe pubsub "/*"
          $ \(PubSubMsg _ _ _ (n :: Int)) -> atomically $ modifyTVar sumVar (+n)

        publish pubsub "/sum" (1 :: Int)
        n <- atomically $ readTVar sumVar
        assertEqual "number should be incremented by all subscriptions" 2 n

      it "publishes messages of different types" $ do
        storage <- memoryStorage
        pubsub <- newPubSubWithStorage storage
        sumVar <- newTVarIO 0
        accVar <- newTVarIO []

        subscribe pubsub "/sum"
          $ \(PubSubMsg _ _ _ (n :: Int)) -> atomically $ modifyTVar sumVar (+n)
        subscribe pubsub "/append"
          $ \(PubSubMsg _ _ _ (x :: Char)) -> atomically $ modifyTVar accVar (x:)

        publish pubsub "/sum" (1 :: Int)
        publish pubsub "/append" 'a'

        result1 <- atomically $ readTVar sumVar
        assertEqual "number should be incremented" 1 result1

        result2 <- atomically $ readTVar accVar
        assertEqual "item should be appended" ['a'] result2

    describe "unsubscribe" $ do

      it "should remove a listener" $ do
        storage <- memoryStorage
        pubsub <- newPubSubWithStorage storage
        sumVar <- newTVarIO 0

        subscribe pubsub "/sum"
          $ \(PubSubMsg _ _ _ (n :: Int)) -> atomically $ modifyTVar sumVar (+n)

        subscribe pubsub "/*"
          $ \(PubSubMsg _ _ _ (n :: Int)) -> atomically $ modifyTVar sumVar (+n)


        publish pubsub "/sum" (1 :: Int)

        unsubscribe pubsub "/sum"

        publish pubsub "/sum" (1 :: Int)

        n <- atomically $ readTVar sumVar
        assertEqual "number should be incremented by all subscriptions" 3 n
