{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}
module Main where

import Paths_bayeux_tester

import Control.Monad (void)

import qualified Data.Aeson.Encode.Pretty as Aeson
import           Data.Char                (toUpper)
import           Data.Monoid              (mconcat)

import qualified Network.HTTP.Types.Status      as HTTP
import qualified Network.Wai                    as Wai
import           Network.Wai.Handler.Warp       (Settings (..), defaultSettings)
import qualified Network.Wai.Handler.Warp       as Warp
import qualified Network.Wai.Handler.WebSockets as Wai
import qualified Network.Wai.Middleware.Static  as Wai
import qualified Network.WebSockets             as WS

import System.IO (BufferMode (LineBuffering), hSetBuffering, stdout)

-- import qualified Rx.Observable as Observable
import qualified Rx.Scheduler as Scheduler

import qualified Tiempo as Tiempo

import qualified Bayeux.PubSub               as Bayeux
import qualified Bayeux.PubSub.RPC           as RPC
import qualified Bayeux.Storage.Core         as Bayeux
import           Bayeux.Transport.Wai        (defaultOptions)
import qualified Bayeux.Transport.Wai        as Bayeux
import qualified Bayeux.Transport.WebSockets as Bayeux

--------------------------------------------------------------------------------

main :: IO ()
main = do
    hSetBuffering stdout LineBuffering

    assetsM <- newAssetsMiddleware

    pubsub <- Bayeux.newPubSub
    bayeuxM <- Bayeux.newBayeuxMiddleware pubsub defaultOptions
    initializePubSub pubsub

    Warp.runSettings (serverSettings pubsub)
      $ assetsM
      $ bayeuxM
      $ handler pubsub
  where
    serverSettings pubsub =
      defaultSettings {
          settingsPort = 3000
        , settingsIntercept =
            Wai.intercept
            $ Bayeux.bayeuxWebSocketMiddleware pubsub defaultOptions
            $ flip WS.rejectRequest "ERROR: Invalid websocket endpoint" }

    newAssetsMiddleware = do
      html_dir <- getDataFileName "html"
      return $ Wai.staticPolicy (Wai.addBase html_dir)

    initializePubSub pubsub = do
      -- Observable.subscribe (Bayeux.clientConnectObservable pubsub)
      --                      (\cid -> putStrLn $ "new clientId: " ++ show cid)
      --                      (\err -> putStrLn "error happened")
      --                      (putStrLn "connect stream DONE")

      -- Observable.subscribe (Bayeux.clientDisconnectObservable pubsub)
      --                      (\cid -> putStrLn $ "disconnected clientId: " ++ show cid)
      --                      (\err -> putStrLn "error happened")
      --                      (putStrLn "disconnect stream DONE")

      Bayeux.subscribe pubsub "/numbers"
        $ \(t :: Bayeux.PubSubMsg Int) -> print (Bayeux._msgPayload t)

      Bayeux.subscribe pubsub "/marco"
        $ \(_ :: Bayeux.PubSubMsg String) -> do
          Bayeux.publish pubsub "/polo" ("Polo" :: String)

      -- RPC.rpcEndpoint pubsub "/upcase"
      --   $ \msg -> do
      --     putStrLn $ "UPCASE: " ++ msg
      --     return . Right $ map toUpper msg

      -- void $
      --   Scheduler.scheduleTimed
      --     Scheduler.newThreadScheduler (Tiempo.seconds 10) $ do
      --       (Right True) <-
      --         RPC.rpcCall pubsub "/append" ("Hello From Haskell" :: String)
      --       putStrLn "DONE"

      -- void $ Scheduler.scheduleTimedRecursive
      --     Scheduler.newThreadScheduler (Tiempo.seconds 10)
      --     $ \recur -> do
      --       mapM_ (Bayeux.publish pubsub "/numbers") ([1..3] :: [Int])
      --       recur

    handler pubsub req =
      case Wai.pathInfo req of
        [] -> do
          return $
            Wai.responseLBS
              HTTP.status200
              [("content-type", "text/html")]
              $ mconcat [
                  "<html>"
                , "<head>"
                , "<script type=\"text/javascript\" src=\"/bayeux/client.js\">"
                , "</script>"
                , "<script type=\"text/javascript\">"
                , "Faye.logger = console;"
                , "var client = new Faye.Client(\"/bayeux\");"
                -- , "client.disable(\"websocket\");"
                , "client.subscribe(\"/polo\","
                , " function(msg) { console.log(\"=> \", msg); });"
                , "</script>"
                , "</head>"
                , "<body>"
                , "<h1>Test Page</h1>"
                , "</body>"
                , "</html>"
                ]

        ["snapshot"] -> do
          storageState <- Bayeux.getStateSnapshot pubsub
          return $ Wai.responseLBS HTTP.status200
                                   [("content-type", "application/json")]
                                   (Aeson.encodePretty storageState)

        _ ->
          return $ Wai.responseLBS HTTP.status400
                                   []
                                   "Bad Request"
