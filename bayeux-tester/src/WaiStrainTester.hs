{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ExtendedDefaultRules #-}
{-# OPTIONS_GHC -fno-warn-type-defaults #-}
module Main where

import Shelly
import Control.Exception (SomeException)
import Control.Monad (sequence, void)
import Control.Monad.Trans (MonadIO(..))
import Control.Concurrent.Async
import Data.Monoid (mconcat)
import Data.Maybe (fromMaybe)
import qualified Data.Text as T
import System.Random (newStdGen, randomR)
default (T.Text)

main :: IO ()
main = shelly $ do
    appendToPath "vendor/slimerjs"
    appendToPath "./.cabal-sandbox/bin/"
    appendToPath "./node_modules/.bin/"
    server <- asyncSh
              $ run_ "bayeux-wai-server" []
    sleep 1
    finally_sh runWorkers
               (liftIO $ cancel server)
  where
    runWorkers = do
      workerCount0 <- get_env "STRAIN_TESTER_WORKERS"
      let workerCount = maybe (4 :: Int) (read . T.unpack) workerCount0

      gen <- liftIO newStdGen
      workers <- mapM (asyncSh . runWorker gen) [1..workerCount]
      void $ liftIO $ waitAnyCancel workers

    runWorker _gen workerId = do
      let workerIdS = T.pack . show $ workerId
      sleep workerId
      echo $ mconcat ["[", workerIdS, "] start bayeux client"]
      catch_sh (silently $ void
                         $ run_ "casperjs"
                                [ "test"
                                , "test/integration_test_client.coffee" ])
               (\(_ :: SomeException) ->
                 echo $ mconcat ["Failure on worker ", workerIdS, ". Skipping"])
      runWorker _gen workerId
