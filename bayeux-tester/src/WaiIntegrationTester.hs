{-# LANGUAGE ExtendedDefaultRules #-}
{-# LANGUAGE OverloadedStrings    #-}
{-# OPTIONS_GHC -fno-warn-type-defaults #-}
module Main where

import Control.Exception (SomeException, try)

import           Control.Concurrent.Async
import           Control.Monad            (void)
import           Control.Monad.Trans      (MonadIO (..))
import           Data.Maybe               (isJust)
import qualified Data.Text                as T
import           Shelly
default (T.Text)

main :: IO ()
main = shelly $ do
    appendToPath "vendor/slimerjs"
    appendToPath "./.cabal-sandbox/bin/"
    appendToPath "./node_modules/.bin/"
    server <- asyncSh
              $ run_ "bayeux-wai-server" []
    sleep 1
    finally_sh runTests
               (liftIO $ cancel server)
  where
    runTests = do
      run_ "casperjs" [ "test"
                      , "test/integration_test_client.coffee" ]

      echo ""
      echo "==============="
      echo ""

      slimerPresent <- isJust <$> get_env "SLIMERJSLAUNCHER"

      when slimerPresent $ do
        run_ "casperjs" [ "--engine=slimerjs"
                        , "test"
                        , "./test/integration_test_client.coffee" ]

        sub $ do
          setenv "TEST_TRANSPORT" "websockets"
          run_ "casperjs" [ "--engine=slimerjs"
                          , "test"
                          , "./test/integration_test_client.coffee" ]
