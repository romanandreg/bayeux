{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}
module Bayeux.Transport.WebSockets where

import Control.Exception   (finally)
import Control.Monad.Trans (lift)

import           Control.Concurrent.STM      (atomically)
import qualified Control.Concurrent.STM.TVar as TVar

import qualified Data.Aeson                 as Aeson
import qualified Data.ByteString.Lazy.Char8 as BL8
import qualified Data.Text.Encoding         as Text

import qualified Network.WebSockets as WS

import qualified Rx.Disposable as Disposable
import qualified Rx.Logger     as Logger
import           Rx.Observable (dispose, subscribe)
import qualified Rx.Observable as Observable

import qualified Tiempo

import Bayeux.Storage.Core (clientErrorObservable, disconnectClient,
                            getClientObservable, handleRequest)
import Bayeux.Types

bayeuxWebSocketMiddleware :: PubSub
                          -> BayeuxTransportOptions
                          -> WS.ServerApp -> WS.ServerApp
bayeuxWebSocketMiddleware pubsub opts baseApp pendingConn = do
    let req = WS.pendingRequest pendingConn
    if WS.requestPath req == (Text.encodeUtf8 $ _bayeuxEndpoint opts)
      then do
        conn           <- WS.acceptRequest pendingConn
        clientIdVar    <- TVar.newTVarIO Nothing
        connDisposable <- Disposable.newCompositeDisposable
        finally (main conn clientIdVar connDisposable)
                (cleanupConn conn clientIdVar connDisposable)
      else
        baseApp pendingConn
  where

    main conn clientIdVar connDisposable = recvLoop
      where
        recvLoop = do
          input <- WS.receiveData conn
          -- ignore malformed json messages
          maybe (return ()) handleWsRequests $ Aeson.decode input
          recvLoop

        handleWsRequests :: [Request Aeson.Value] -> IO ()
        handleWsRequests reqList@([]) =
          WS.sendTextData conn $ Aeson.encode reqList
        handleWsRequests reqList =
          mapM_ handleWsRequest reqList

        handleWsRequest :: Request Aeson.Value -> IO ()
        handleWsRequest req@(ReqConnect _ _ clientId WebSocket) =
          setupClientIdPublishSubscription req clientId
        handleWsRequest (ReqConnect _ _ _clientId _transport) =
          -- NOTE: do not handle request, it is meant for another
          -- transport
          -- setupClientIdPublishSubscription req clientId
          return ()
        handleWsRequest req = handleRequest pubsub req

        setupClientIdPublishSubscription connReq clientId = do
          mCurrentClientId <- atomically $ TVar.readTVar clientIdVar
          case mCurrentClientId of
            -- receiving another connect request from same client, just ignore
            Just clientId'
              | clientId' == clientId -> return ()
              | otherwise ->
                 -- this shouldn't happen, a connection request was sent
                 -- when a connection was already established
                 Logger.scope pubsub 'bayeuxWebSocketMiddleware $
                   Logger.warn
                     ("Received unexpected Connect request on WS connection" :: String)
            Nothing -> do
              -- receiving a connect request for the first time
              -- start stream setup

              -- NOTE: This subscription checks errors that might
              -- happen when a clientId is unknown, this happens when
              -- there is a reset on the server state with a transient
              -- storage (e.g. memory)
              clientErrDisp <-
                subscribe
                  (Observable.timeoutAfterFirst
                      (Tiempo.milliSeconds 100)
                      (clientErrorObservable pubsub clientId))
                  (WS.sendTextData conn . Aeson.encode)
                  (const $ return ())
                  (return ())

              Disposable.append clientErrDisp connDisposable

              -- IMPORTANT: we handle request before getting
              -- clientObservable because a req will likely create the
              -- actual record in the storage
              handleRequest pubsub connReq

              mClientObservable <- getClientObservable pubsub clientId
              case mClientObservable of
                Nothing -> do
                  -- we got a Connect request with an invalid clientId
                  -- the previos clientErr subscription is going to
                  -- handle this error
                  Logger.scope pubsub 'bayeuxWebSocketMiddleware $
                    Logger.warnFormat
                      "[clientId={}] Received Connect request from invalid clientId"
                      (Logger.Only clientId)
                  return ()

                Just clientObservable -> do
                  -- we got a new clientSubject, and with that a new
                  -- established client connection
                  atomically $ TVar.writeTVar clientIdVar (Just clientId)

                  wsBridgeDisp <-
                    subscribe
                      clientObservable
                      (WS.sendTextData conn . Aeson.encode)
                      (\err ->
                          Logger.scope pubsub 'bayeuxWebSocketMiddleware $
                            Logger.severe
                              $ "Bayeux: error on bayeux websocket loop: " ++ show err)
                      (Logger.scope pubsub 'bayeuxWebSocketMiddleware $
                        Logger.traceFormat
                          "[clientId={}] client websocket loop is done."
                          (Logger.Only clientId))

                  clientDisp <-
                    Disposable.createDisposable
                      $ disconnectClient pubsub clientId

                  Disposable.append wsBridgeDisp connDisposable
                  Disposable.append clientDisp connDisposable

    cleanupConn conn clientIdVar connDisposable = do
      Logger.scope pubsub 'bayeuxWebSocketMiddleware $ do
        mClientId <- lift $ atomically $ TVar.readTVar clientIdVar
        Logger.traceFormat "[clientId={}] Cleaning websocket connection"
                           (Logger.Only mClientId)
        lift $ do
          WS.sendClose conn ("Bayeux: closing... server cleanup" :: BL8.ByteString)
          dispose connDisposable
