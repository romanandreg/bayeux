# Summary

Implementation of the [Bayeux protocol](http://svn.cometd.org/trunk/bayeux/bayeux.html)
in Haskell. This library is divided in many projects:

* bayeux

Core library that has Bayeux PubSub and modules that would be used from
other extensions

* bayeux-transport-wai

Offers WAI/Warp middleware for long-polling and callback-polling, tested
with [faye browser client](https://github.com/faye/faye).

* bayeux-transport-websockets

Offers websocket middleware, tested with [faye browser client](https://github.com/faye/faye).

* bayeux-examples

Examples of bayeux usage.

# Authors

This is library is written and mantained by Roman Gonzalez, <romanandreg@gmail.com>

# License

The MIT License (MIT)

Copyright (c) 2014 Roman Gonzalez.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
