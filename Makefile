recurfind = $(shell find $(1) -name '$(2)')

SLIMERJS_BIN = vendor/slimerjs/slimerjs

CABAL_BIN = cabal --sandbox-config-file=../cabal.sandbox.config
CABAL_INSTALL_FLAGS = --enable-tests --force-reinstalls

BAYEUX_SOURCES    = $(call recurfind,bayeux/src,[^.]*.hs) $(call recurfind,bayeux/test,[^.]*.hs)
BAYEUX_WAI_SOURCES = $(call recurfind,bayeux-transport-wai/src,[^.]*.hs)
BAYEUX_WS_SOURCES = $(call recurfind,bayeux-transport-websockets/src,[^.]*.hs)
BAYEUX_TESTER_SOURCES = $(call recurfind,bayeux-tester/src,[^.]*.hs)
ALL_SOURCES = $(BAYEUX_SOURCES) $(BAYEUX_WAI_SOURCES) $(BAYEUX_WS_SOURCES) $(BAYEUX_TESTER_SOURCES)

BAYEUX_CORE_TEST_BIN = bayeux/dist/build/bayeux-tests/bayeux-tests
BAYEUX_WAI_SERVER = .cabal-sandbox/bin/bayeux-wai-server

.repos:
	[[ -d vendor ]] || mkdir vendor

	[[ -d vendor/rxhs ]] || \
	{ cd vendor; git clone -b development https://bitbucket.org/romanandreg/rxhs; }

	[[ -d vendor/rxhs-logger ]] || \
	{ cd vendor; git clone -b development https://bitbucket.org/romanandreg/rxhs-logger; }

	[[ -d vendor/faye ]] || \
	{ cd vendor; git clone https://github.com/roman/faye; }

	touch .repos

.initialize: .repos
	$(CABAL_BIN) sandbox init
	$(CABAL_BIN) sandbox add-source vendor/rxhs
	$(CABAL_BIN) sandbox add-source vendor/rxhs-logger
	$(CABAL_BIN) sandbox add-source bayeux
	$(CABAL_BIN) sandbox add-source bayeux-transport-wai
	$(CABAL_BIN) sandbox add-source bayeux-transport-websockets
	npm install
	touch .initialize

################################################################################

.setup: .initialize .repos bayeux/.setup bayeux-transport-wai/.setup bayeux-transport-websockets/.setup
	touch .setup

bayeux/.setup:
	cd bayeux; \
	$(CABAL_BIN) sandbox --sandbox=../.cabal-sandbox init; \
	$(CABAL_BIN) install $(CABAL_INSTALL_FLAGS); \
	touch .setup

bayeux-transport-wai/.setup:
	cd bayeux-transport-wai/; \
	$(CABAL_BIN) sandbox --sandbox=../.cabal-sandbox init; \
	$(CABAL_BIN) install $(CABAL_INSTALL_FLAGS); \
	touch .setup

bayeux-transport-websockets/.setup:
	cd bayeux-transport-websockets/; \
	$(CABAL_BIN) sandbox --sandbox=../.cabal-sandbox init; \
	$(CABAL_BIN) install $(CABAL_INSTALL_FLAGS); \
	touch .setup

bayeux-tester/.setup:
	cd bayeux-tester; \
	$(CABAL_BIN) sandbox --sandbox=../.cabal-sandbox init; \
	$(CABAL_BIN) install $(CABAL_INSTALL_FLAGS); \
	touch .setup

.PHONY: setup
setup: .setup

################################################################################

.PHONY: clean
clean:
	cd bayeux; $(CABAL_BIN) clean
	cd bayeux-transport-wai; $(CABAL_BIN) clean
	cd bayeux-transport-websockets; $(CABAL_BIN) clean
	cd bayeux-tester; $(CABAL_BIN) clean
	rm .cabal-sandbox/bin/*

.PHONY: unsetup
unsetup:
	-rm bayeux/.setup
	-rm bayeux-transport-wai/.setup
	-rm bayeux-transport-websockets/.setup
	-rm bayeux-tester/.setup
	-rm .setup

.PHONY: purge
purge: unsetup
	-rm -rf node_modules
	-rm -rf vendor
	-rm .repos
	-rm .initialize
	$(CABAL_BIN) sandbox delete

################################################################################

$(BAYEUX_WAI_SERVER): .setup $(ALL_SOURCES)
	cd bayeux-tester; $(CABAL_BIN) install $(CABAL_INSTALL_FLAGS)

.PHONY: install
install: $(BAYEUX_WAI_SERVER)


################################################################################

bayeux/.installed:
	cd bayeux; \
	$(CABAL_BIN) install --enable-tests; \
	touch .installed


$(BAYEUX_CORE_TEST_BIN):  bayeux/.installed $(BAYEUX_SOURCES)
	cd bayeux; \
	$(CABAL_BIN) configure --enable-tests; \
	$(CABAL_BIN) build

.PHONY: build_bayeux_test
build_bayeux_test: $(BAYEUX_CORE_TEST_BIN)

################################################################################

$(SLIMERJS_BIN):
	cd vendor; \
	wget http://download.slimerjs.org/v0.9/0.9.0/slimerjs-0.9.0.zip; \
	unzip slimerjs-0.9.0.zip; \
	ln -s slimerjs-0.9.0 slimerjs

.PHONY: test_bayeux
test_bayeux : .setup build_bayeux_test
	@echo ""
	@echo "===== Test Bayeux Core API"
	@./$(BAYEUX_CORE_TEST_BIN)

.PHONY: test_wai_transport
test_wai_transport: .setup $(SLIMERJS_BIN) $(BAYEUX_WAI_SERVER)
	@echo ""
	@echo "===== Test Bayeux WAI transport with CasperJS"
	@.cabal-sandbox/bin/wai-integration-tester

.PHONY: test
test: .setup test_bayeux test_wai_transport
