# function debugPage() {
#   console.log("Refresh a second debugger-port page and open a second webkit inspector for the target page.");
#   console.log("Letting this page continue will then trigger a break in the target page.");
#   debugger; // pause here in first web browser tab for steps 5 & 6
#   casper.evaluate(function() {
#     debugger; // step 7 will wait here in the second web browser tab
#   });
# }

system = require("system")
TEST_TRANSPORT = system.env.TEST_TRANSPORT or "polling"

bayeux_roundtrip_test = (desc_str, test_url) ->

  casper.test.begin desc_str, (test) ->

    casper.start test_url, ->
      test.assertTitle("Bayeux Roundtrip Test", "Test page loaded")
      test.assertEval((-> client != undefined),  "Bayeux client is present")

    casper.thenEvaluate ->
      client.__pong_count = 5
      client.subscribe "/polo", (msg) ->
        if client.__pong_count > 0
          client.publish "/marco", "MARCO"
          client.__pong_count = client.__pong_count - 1

    casper.wait 300, ->
      casper.evaluate(-> client.publish("/marco", "Marco"))

    casper.wait 300, ->
      test.assertEvalEqual((-> client.__pong_count), 0, "Bayeux roundtrip successful")

    casper.run ->
      test.done()
      test.renderResults(true)


if TEST_TRANSPORT == "polling"
  bayeux_roundtrip_test("test bayeux roundtrip using polling",
                        "http://localhost:3000/index.html?disable_websockets=1")

else if TEST_TRANSPORT == "websockets"
  bayeux_roundtrip_test("test bayeux roundtrip using websockets",
                        "http://localhost:3000/index.html")
else
  console.log("Unknown transport: ", TEST_TRANSPORT)
